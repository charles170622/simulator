package com.formssi.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.formssi.bean.FileDataBean;
import com.formssi.bean.SimCTOReceiveBean;
import com.formssi.defines.SimConstants;

public class SimFileHandleUtils {

	/**
	 * read local file
	 */
	public static FileDataBean readFile(InputStream in) throws IOException, InvalidFormatException, ParseException {

		FileDataBean fb = new FileDataBean();

		XSSFWorkbook xWorkbook = new XSSFWorkbook(in);
		// CTO
		XSSFSheet sheet1 = xWorkbook.getSheetAt(0);
		// DDO
		XSSFSheet sheet2 = xWorkbook.getSheetAt(1);
		// RRO
		XSSFSheet sheet3 = xWorkbook.getSheetAt(2);

		fb.setMapListCto(getMapList(sheet1));
		fb.setMapListDdo(getMapList(sheet2));
		fb.setMapListRro(getMapList(sheet3));

		xWorkbook.close();
		return fb;
	}
	
	public static void createReceiveFile(String dirName) throws IOException {
		Workbook wb = new XSSFWorkbook();
		XSSFSheet sheet1 = (XSSFSheet) wb.createSheet("sheet1");
		FileOutputStream fileOut = new FileOutputStream(SimConstants.BANKSIM_PATH_RECEIVE_ACK + dirName + "/" + dirName + "_ACK.xlsx");
		
		//write field
		Field[] fields = SimCTOReceiveBean.class.getDeclaredFields();
		Row row = sheet1.createRow(0);
		for(int i=0; i<fields.length - 1; i++){
			 Field field = fields[i];
			 field.setAccessible(true);
             Cell cell = row.createCell(i);
             String name = field.getName();
             cell. setCellType(CellType.STRING);;
             cell.setCellValue(name);
		}
		wb.write(fileOut);
		fileOut.close();
	}
	

	private static List<Map<String, Object>> getMapList(XSSFSheet sheet) {
		List<Map<String, Object>> mapList = new ArrayList<>();

		List<String> fList = new ArrayList<>();
		List<List<String>> vLists = new ArrayList<>();

		// empty sheet
		if (sheet.getLastRowNum() == 0 && sheet.getPhysicalNumberOfRows() == 0) {
			return mapList;
		}

		XSSFRow row = sheet.getRow(0);
		for (int i = 0; i < row.getLastCellNum(); i++) {
			XSSFCell cell = row.getCell(i);
			cell.setCellType(CellType.STRING);
			fList.add(cell.getStringCellValue().trim());
		}
		// get values
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			List<String> tempList = new ArrayList<>();
			XSSFRow r = sheet.getRow(i);
			if (isRowEmpty(r)) {
				continue;
			} else {
				String content = "";
				for (int j = 0; j < r.getLastCellNum(); j++) {
					XSSFCell cell = r.getCell(j);
					if (cell != null) {
						cell.setCellType(CellType.STRING);
						content = cell.getStringCellValue();
						tempList.add(content.trim());
					}
				}
				vLists.add(tempList);
			}
		}

		for (int p = 0; p < vLists.size(); p++) {
			List<String> vList = vLists.get(p);
			Map<String, Object> linkedMap = new LinkedHashMap<>();

			// 确保以最小的list集合长度为rs的长度, 防止 NullPointerException 异常
			int keyLen = (fList != null) ? fList.size() : 0;
			int valLen = (vList != null) ? vList.size() : 0;
			int len = keyLen;
			if (len > valLen) {
				len = valLen;
			}
			for (int k = 0; k < len; k++) {
				linkedMap.put(fList.get(k), vList.get(k));
			}

			mapList.add(linkedMap);
		}
		return mapList;
	}

	/**
	 * if xlsx row is empty
	 */
	private static boolean isRowEmpty(Row row) {

		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellTypeEnum() != CellType.BLANK)
				return false;
		}
		return true;
	}
	
	public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

	
	 
	
}
