package com.formssi.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SimFileNameUtils {
	
	public static long generateRandomNumber(int n){
        if(n<1){
            throw new IllegalArgumentException("随机数位数必须大于0");
        }
        return (long)(Math.random()*9*Math.pow(10,n-1)) + (long)Math.pow(10,n-1);
	}

	
	public static String getFileName(String bizSvc, String txId){
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		return "FPS" + bizSvc.substring(3) + "_" + txId + "_" + df.format(new Date()) + generateRandomNumber(3) + ".xml";
	}
}
