package com.formssi.business.common.entity;

public class SimVO_Pacs003_DrctDbtTxInf
{
	private String pmtIdInstrId;
	private String pmtIdEndToEndId;
	private String pmtIdTxId;
	private String clrSysRef;
	
	private String pmtTpInfCtgyPrtry;
	private String intrBkSttlmCurrency;
	private String intrBkSttlmAmt;
	private String intrBkSttlmDt;
	private String instdCurrency;
	private String instdAmt;

	private String chrgBr;
	private String chrgAgentId;
	private String chrgAgentBic;
	private String chrgCcy;
	private String chrgAmount;
	
	private String cdtrNm;
	private String cdtrOrgIdAnyBIC;
	private String cdtrOrgIdOthrId;
	private String cdtrOrgIdOthrIdSchmeNm;
	private String cdtrOrgIdOthrIssr;
	private String cdtrPrvtIdOthrId;
	private String cdtrPrvtIdOthrIdSchmeNm;
	private String cdtrPrvtIdOthrIssr;
	
	private String cdtrAcctIdOthId;
	private String cdtrAcctIdOthSchPrtry;
	private String cdtrAgtFiClrMmbId;

	private String dbtrNm;
	private String dbtrOrgIdAnyBIC; // BIC
	private String dbtrOrgIdOthrId;
	private String dbtrOrgIdOthrIdSchmeNm;
	private String dbtrOrgIdOthrIssr;
	private String dbtrPrvtIdOthrId;
	private String dbtrPrvtIdOthrIdSchmeNm;
	private String dbtrPrvtIdOthrIssr;
	
	private String dbtrAcctIdOthId;
	private String dbtrAcctIdOthSchPrtry;
	private String dbtrAgtFiClrMmbId;
	private String drctDbtTxRltId;
	private String dbtrContPhone;
	private String dbtrContEmailAddr;
	
	private String dbtrAgtBic;
	private String cdtrAgtBic;
	private String cdtrContPhone;
	private String cdtrContEmailAddr;
	private String paymentPurposeType;
	private String paymentPurposeCd;
	private String paymentPurposeProprietary;
	private String remittanceInformation;
	
	
	public String getPmtIdInstrId() {
		return pmtIdInstrId;
	}
	public void setPmtIdInstrId(String pmtIdInstrId) {
		this.pmtIdInstrId = pmtIdInstrId;
	}
	public String getPmtIdEndToEndId() {
		return pmtIdEndToEndId;
	}
	public void setPmtIdEndToEndId(String pmtIdEndToEndId) {
		this.pmtIdEndToEndId = pmtIdEndToEndId;
	}
	public String getPmtIdTxId() {
		return pmtIdTxId;
	}
	public void setPmtIdTxId(String pmtIdTxId) {
		this.pmtIdTxId = pmtIdTxId;
	}
	public String getClrSysRef() {
		return clrSysRef;
	}
	public void setClrSysRef(String clrSysRef) {
		this.clrSysRef = clrSysRef;
	}
	public String getPmtTpInfCtgyPrtry() {
		return pmtTpInfCtgyPrtry;
	}
	public void setPmtTpInfCtgyPrtry(String pmtTpInfCtgyPrtry) {
		this.pmtTpInfCtgyPrtry = pmtTpInfCtgyPrtry;
	}
	public String getIntrBkSttlmCurrency() {
		return intrBkSttlmCurrency;
	}
	public void setIntrBkSttlmCurrency(String intrBkSttlmCurrency) {
		this.intrBkSttlmCurrency = intrBkSttlmCurrency;
	}
	public String getIntrBkSttlmAmt() {
		return intrBkSttlmAmt;
	}
	public void setIntrBkSttlmAmt(String intrBkSttlmAmt) {
		this.intrBkSttlmAmt = intrBkSttlmAmt;
	}
	public String getIntrBkSttlmDt() {
		return intrBkSttlmDt;
	}
	public void setIntrBkSttlmDt(String intrBkSttlmDt) {
		this.intrBkSttlmDt = intrBkSttlmDt;
	}
	public String getInstdCurrency() {
		return instdCurrency;
	}
	public void setInstdCurrency(String instdCurrency) {
		this.instdCurrency = instdCurrency;
	}
	public String getInstdAmt() {
		return instdAmt;
	}
	public void setInstdAmt(String instdAmt) {
		this.instdAmt = instdAmt;
	}
	public String getChrgBr() {
		return chrgBr;
	}
	public void setChrgBr(String chrgBr) {
		this.chrgBr = chrgBr;
	}
	public String getChrgAgentId() {
		return chrgAgentId;
	}
	public void setChrgAgentId(String chrgAgentId) {
		this.chrgAgentId = chrgAgentId;
	}
	public String getChrgAgentBic() {
		return chrgAgentBic;
	}
	public void setChrgAgentBic(String chrgAgentBic) {
		this.chrgAgentBic = chrgAgentBic;
	}
	public String getChrgCcy() {
		return chrgCcy;
	}
	public void setChrgCcy(String chrgCcy) {
		this.chrgCcy = chrgCcy;
	}
	public String getChrgAmount() {
		return chrgAmount;
	}
	public void setChrgAmount(String chrgAmount) {
		this.chrgAmount = chrgAmount;
	}
	public String getCdtrNm() {
		return cdtrNm;
	}
	public void setCdtrNm(String cdtrNm) {
		this.cdtrNm = cdtrNm;
	}
	public String getCdtrOrgIdAnyBIC() {
		return cdtrOrgIdAnyBIC;
	}
	public void setCdtrOrgIdAnyBIC(String cdtrOrgIdAnyBIC) {
		this.cdtrOrgIdAnyBIC = cdtrOrgIdAnyBIC;
	}
	public String getCdtrOrgIdOthrId() {
		return cdtrOrgIdOthrId;
	}
	public void setCdtrOrgIdOthrId(String cdtrOrgIdOthrId) {
		this.cdtrOrgIdOthrId = cdtrOrgIdOthrId;
	}
	public String getCdtrOrgIdOthrIdSchmeNm() {
		return cdtrOrgIdOthrIdSchmeNm;
	}
	public void setCdtrOrgIdOthrIdSchmeNm(String cdtrOrgIdOthrIdSchmeNm) {
		this.cdtrOrgIdOthrIdSchmeNm = cdtrOrgIdOthrIdSchmeNm;
	}
	public String getCdtrOrgIdOthrIssr() {
		return cdtrOrgIdOthrIssr;
	}
	public void setCdtrOrgIdOthrIssr(String cdtrOrgIdOthrIssr) {
		this.cdtrOrgIdOthrIssr = cdtrOrgIdOthrIssr;
	}
	public String getCdtrPrvtIdOthrId() {
		return cdtrPrvtIdOthrId;
	}
	public void setCdtrPrvtIdOthrId(String cdtrPrvtIdOthrId) {
		this.cdtrPrvtIdOthrId = cdtrPrvtIdOthrId;
	}
	public String getCdtrPrvtIdOthrIdSchmeNm() {
		return cdtrPrvtIdOthrIdSchmeNm;
	}
	public void setCdtrPrvtIdOthrIdSchmeNm(String cdtrPrvtIdOthrIdSchmeNm) {
		this.cdtrPrvtIdOthrIdSchmeNm = cdtrPrvtIdOthrIdSchmeNm;
	}
	public String getCdtrPrvtIdOthrIssr() {
		return cdtrPrvtIdOthrIssr;
	}
	public void setCdtrPrvtIdOthrIssr(String cdtrPrvtIdOthrIssr) {
		this.cdtrPrvtIdOthrIssr = cdtrPrvtIdOthrIssr;
	}
	public String getCdtrAcctIdOthId() {
		return cdtrAcctIdOthId;
	}
	public void setCdtrAcctIdOthId(String cdtrAcctIdOthId) {
		this.cdtrAcctIdOthId = cdtrAcctIdOthId;
	}
	public String getCdtrAcctIdOthSchPrtry() {
		return cdtrAcctIdOthSchPrtry;
	}
	public void setCdtrAcctIdOthSchPrtry(String cdtrAcctIdOthSchPrtry) {
		this.cdtrAcctIdOthSchPrtry = cdtrAcctIdOthSchPrtry;
	}
	public String getCdtrAgtFiClrMmbId() {
		return cdtrAgtFiClrMmbId;
	}
	public void setCdtrAgtFiClrMmbId(String cdtrAgtFiClrMmbId) {
		this.cdtrAgtFiClrMmbId = cdtrAgtFiClrMmbId;
	}
	public String getDbtrNm() {
		return dbtrNm;
	}
	public void setDbtrNm(String dbtrNm) {
		this.dbtrNm = dbtrNm;
	}
	public String getDbtrOrgIdAnyBIC() {
		return dbtrOrgIdAnyBIC;
	}
	public void setDbtrOrgIdAnyBIC(String dbtrOrgIdAnyBIC) {
		this.dbtrOrgIdAnyBIC = dbtrOrgIdAnyBIC;
	}
	public String getDbtrOrgIdOthrId() {
		return dbtrOrgIdOthrId;
	}
	public void setDbtrOrgIdOthrId(String dbtrOrgIdOthrId) {
		this.dbtrOrgIdOthrId = dbtrOrgIdOthrId;
	}
	public String getDbtrOrgIdOthrIdSchmeNm() {
		return dbtrOrgIdOthrIdSchmeNm;
	}
	public void setDbtrOrgIdOthrIdSchmeNm(String dbtrOrgIdOthrIdSchmeNm) {
		this.dbtrOrgIdOthrIdSchmeNm = dbtrOrgIdOthrIdSchmeNm;
	}
	public String getDbtrOrgIdOthrIssr() {
		return dbtrOrgIdOthrIssr;
	}
	public void setDbtrOrgIdOthrIssr(String dbtrOrgIdOthrIssr) {
		this.dbtrOrgIdOthrIssr = dbtrOrgIdOthrIssr;
	}
	public String getDbtrPrvtIdOthrId() {
		return dbtrPrvtIdOthrId;
	}
	public void setDbtrPrvtIdOthrId(String dbtrPrvtIdOthrId) {
		this.dbtrPrvtIdOthrId = dbtrPrvtIdOthrId;
	}
	public String getDbtrPrvtIdOthrIdSchmeNm() {
		return dbtrPrvtIdOthrIdSchmeNm;
	}
	public void setDbtrPrvtIdOthrIdSchmeNm(String dbtrPrvtIdOthrIdSchmeNm) {
		this.dbtrPrvtIdOthrIdSchmeNm = dbtrPrvtIdOthrIdSchmeNm;
	}
	public String getDbtrPrvtIdOthrIssr() {
		return dbtrPrvtIdOthrIssr;
	}
	public void setDbtrPrvtIdOthrIssr(String dbtrPrvtIdOthrIssr) {
		this.dbtrPrvtIdOthrIssr = dbtrPrvtIdOthrIssr;
	}
	public String getDbtrAcctIdOthId() {
		return dbtrAcctIdOthId;
	}
	public void setDbtrAcctIdOthId(String dbtrAcctIdOthId) {
		this.dbtrAcctIdOthId = dbtrAcctIdOthId;
	}
	public String getDbtrAcctIdOthSchPrtry() {
		return dbtrAcctIdOthSchPrtry;
	}
	public void setDbtrAcctIdOthSchPrtry(String dbtrAcctIdOthSchPrtry) {
		this.dbtrAcctIdOthSchPrtry = dbtrAcctIdOthSchPrtry;
	}
	public String getDbtrAgtFiClrMmbId() {
		return dbtrAgtFiClrMmbId;
	}
	public void setDbtrAgtFiClrMmbId(String dbtrAgtFiClrMmbId) {
		this.dbtrAgtFiClrMmbId = dbtrAgtFiClrMmbId;
	}
	public String getDrctDbtTxRltId() {
		return drctDbtTxRltId;
	}
	public void setDrctDbtTxRltId(String drctDbtTxRltId) {
		this.drctDbtTxRltId = drctDbtTxRltId;
	}
	public String getDbtrContPhone() {
		return dbtrContPhone;
	}
	public void setDbtrContPhone(String dbtrContPhone) {
		this.dbtrContPhone = dbtrContPhone;
	}
	public String getDbtrContEmailAddr() {
		return dbtrContEmailAddr;
	}
	public void setDbtrContEmailAddr(String dbtrContEmailAddr) {
		this.dbtrContEmailAddr = dbtrContEmailAddr;
	}
	public String getDbtrAgtBic() {
		return dbtrAgtBic;
	}
	public void setDbtrAgtBic(String dbtrAgtBic) {
		this.dbtrAgtBic = dbtrAgtBic;
	}
	public String getCdtrAgtBic() {
		return cdtrAgtBic;
	}
	public void setCdtrAgtBic(String cdtrAgtBic) {
		this.cdtrAgtBic = cdtrAgtBic;
	}
	public String getCdtrContPhone() {
		return cdtrContPhone;
	}
	public void setCdtrContPhone(String cdtrContPhone) {
		this.cdtrContPhone = cdtrContPhone;
	}
	public String getCdtrContEmailAddr() {
		return cdtrContEmailAddr;
	}
	public void setCdtrContEmailAddr(String cdtrContEmailAddr) {
		this.cdtrContEmailAddr = cdtrContEmailAddr;
	}
	public String getPaymentPurposeType() {
		return paymentPurposeType;
	}
	public void setPaymentPurposeType(String paymentPurposeType) {
		this.paymentPurposeType = paymentPurposeType;
	}
	public String getPaymentPurposeCd() {
		return paymentPurposeCd;
	}
	public void setPaymentPurposeCd(String paymentPurposeCd) {
		this.paymentPurposeCd = paymentPurposeCd;
	}
	public String getPaymentPurposeProprietary() {
		return paymentPurposeProprietary;
	}
	public void setPaymentPurposeProprietary(String paymentPurposeProprietary) {
		this.paymentPurposeProprietary = paymentPurposeProprietary;
	}
	public String getRemittanceInformation() {
		return remittanceInformation;
	}
	public void setRemittanceInformation(String remittanceInformation) {
		this.remittanceInformation = remittanceInformation;
	}

	
}
