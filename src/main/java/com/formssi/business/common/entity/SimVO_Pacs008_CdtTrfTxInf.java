package com.formssi.business.common.entity;


public class SimVO_Pacs008_CdtTrfTxInf
{
	// Payment Identification
	private String instrId; // Instruction Id
	private String endToEndId; // End-to-end Id
	private String txId; // Transaction Id
	private String clrSysRef; // FPS Reference Number

	// Payment Type Information
	private String lclInstrm; // Account verification
	private String ctgyPurp; // Payment Category Purpose of FPS

	private String intrBkSttlmAmt; // Interbank Settlement Amount
	private String intrBkSttlmAmtCcy; // Interbank Settlement Amount Currency

	private String intrBkSttlmDt; // Interbank Settlement Date

	private String sttlmTmIndctn; // Settlement time indication->Credit date time:for
							// Sc.C only

	private String instdAmt; // Instructed Amount
	private String instdAmtCcy; // Instructed Amount Currency

	private String chrgBr; // Charge Bearer
	private String chrgAgentId;
	private String chrgAgentBic;
	private String chrgCcy;
	private String chrgAmount;

	// Debtor
	private String dbtrNm; // Debtor name
	// Debtor->Identification->Organisation
	private String dbtrOrgIdAnyBIC; // BIC
	private String dbtrOrgIdOthrId;
	private String dbtrOrgIdOthrIdSchmeNm;
	private String dbtrOrgIdOthrIssr;
	private String dbtrPrvtIdOthrId;
	private String dbtrPrvtIdOthrIdSchmeNm;
	private String dbtrPrvtIdOthrIssr;
	// Debtor->Contact Details
	private String dbtrMobNb; // Phone number
	private String dbtrEmailAdr; // Email address

	// Debtor Account
	private String dbtrAcctId; // Debtor Account Identification
	private String dbtrAccSchmeNm; // Debtor Account Scheme Name

	// Debtor Agent
	private String dbtrAgBICFI; // Debtor Agent BICFI
	private String dbtrAgClrSysMmbId; // Debtor Agent Clearing System Member
										// Identification

	// Creditor Agent
	private String cdtrAgtBICFI; // Creditor Agent BICFI
	private String cdtrAgtClrSysMmbId; // Creditor Agent Clearing System Member
										// Identification

	// Creditor
	private String cdtrNm; // Creditor name
	private String cdtrOrgIdAnyBIC;
	private String cdtrOrgIdOthrId;
	private String cdtrOrgIdOthrIdSchmeNm;
	private String cdtrOrgIdOthrIssr;
	private String cdtrPrvtIdOthrId;
	private String cdtrPrvtIdOthrIdSchmeNm;
	private String cdtrPrvtIdOthrIssr;

	// Creditor->Contact Details
	private String cdtrMobNb;
	private String cdtrEmailAdr;

	// Creditor Account
	private String cdtrAcctId;
	private String cdtrAcctSchmeNm;

	// Purpose
	private String purpCd;
	private String purpPrtry;
                   
	// Remittance Information
	private String rmtInf;

	public class ChargeInfo{
		
		private String chrgsInfAmt;
		
		private String chrgsInfCcy;
		
		private String chrgsInfAgtBICFI;
		
		private String chrgsInfAgtClrSysId;
		
		public String getChrgsInfAmt() {
			return chrgsInfAmt;
		}
		public void setChrgsInfAmt(String chrgsInfAmt) {
			this.chrgsInfAmt = chrgsInfAmt;
		}
		public String getChrgsInfCcy() {
			return chrgsInfCcy;
		}
		public void setChrgsInfCcy(String chrgsInfCcy) {
			this.chrgsInfCcy = chrgsInfCcy;
		}
		public String getChrgsInfAgtBICFI() {
			return chrgsInfAgtBICFI;
		}
		public void setChrgsInfAgtBICFI(String chrgsInfAgtBICFI) {
			this.chrgsInfAgtBICFI = chrgsInfAgtBICFI;
		}
		public String getChrgsInfAgtClrSysId() {
			return chrgsInfAgtClrSysId;
		}
		public void setChrgsInfAgtClrSysId(String chrgsInfAgtClrSysId) {
			this.chrgsInfAgtClrSysId = chrgsInfAgtClrSysId;
		}

		
	}

	public class CdtrOrgIdOth
	{
		private String cdtrOrgIdId;
		
		private String cdtrOrgIdSchmeNm;
		
		private String cdtrOrgIdIssr;
		
		public String getCdtrOrgIdId() {
			return cdtrOrgIdId;
		}
		public void setCdtrOrgIdId(String cdtrOrgIdId) {
			this.cdtrOrgIdId = cdtrOrgIdId;
		}
		public String getCdtrOrgIdSchmeNm() {
			return cdtrOrgIdSchmeNm;
		}
		public void setCdtrOrgIdSchmeNm(String cdtrOrgIdSchmeNm) {
			this.cdtrOrgIdSchmeNm = cdtrOrgIdSchmeNm;
		}
		public String getCdtrOrgIdIssr() {
			return cdtrOrgIdIssr;
		}
		public void setCdtrOrgIdIssr(String cdtrOrgIdIssr) {
			this.cdtrOrgIdIssr = cdtrOrgIdIssr;
		}

		
	}

	public class CdtrPrvtIdOth
	{
		private String cdtrPrvtIdId;
		
		private String cdtrPrvtIdSchmeNm;
		
		private String cdtrPrvtIdIssr;
		
		public String getCdtrPrvtIdId() {
			return cdtrPrvtIdId;
		}
		public void setCdtrPrvtIdId(String cdtrPrvtIdId) {
			this.cdtrPrvtIdId = cdtrPrvtIdId;
		}
		public String getCdtrPrvtIdSchmeNm() {
			return cdtrPrvtIdSchmeNm;
		}
		public void setCdtrPrvtIdSchmeNm(String cdtrPrvtIdSchmeNm) {
			this.cdtrPrvtIdSchmeNm = cdtrPrvtIdSchmeNm;
		}
		public String getCdtrPrvtIdIssr() {
			return cdtrPrvtIdIssr;
		}
		public void setCdtrPrvtIdIssr(String cdtrPrvtIdIssr) {
			this.cdtrPrvtIdIssr = cdtrPrvtIdIssr;
		}
	}

	public class DbtrOrgIdOth
	{
		private String dbtrOrgIdId;
		
		private String dbtrOrgIdSchmeNm;
		
		private String dbtrOrgIdIssr;

		public String getDbtrOrgIdId() {
			return dbtrOrgIdId;
		}

		public void setDbtrOrgIdId(String dbtrOrgIdId) {
			this.dbtrOrgIdId = dbtrOrgIdId;
		}

		public String getDbtrOrgIdSchmeNm() {
			return dbtrOrgIdSchmeNm;
		}

		public void setDbtrOrgIdSchmeNm(String dbtrOrgIdSchmeNm) {
			this.dbtrOrgIdSchmeNm = dbtrOrgIdSchmeNm;
		}

		public String getDbtrOrgIdIssr() {
			return dbtrOrgIdIssr;
		}

		public void setDbtrOrgIdIssr(String dbtrOrgIdIssr) {
			this.dbtrOrgIdIssr = dbtrOrgIdIssr;
		}

		
	}

	public class DbtrPrvtIdOth
	{
		private String dbtrPrvtIdId;
		
		private String dbtrPrvtIdSchmeNm;
		
		private String dbtrPrvtIdIssr;

		public String getDbtrPrvtIdId() {
			return dbtrPrvtIdId;
		}

		public void setDbtrPrvtIdId(String dbtrPrvtIdId) {
			this.dbtrPrvtIdId = dbtrPrvtIdId;
		}

		public String getDbtrPrvtIdSchmeNm() {
			return dbtrPrvtIdSchmeNm;
		}

		public void setDbtrPrvtIdSchmeNm(String dbtrPrvtIdSchmeNm) {
			this.dbtrPrvtIdSchmeNm = dbtrPrvtIdSchmeNm;
		}

		public String getDbtrPrvtIdIssr() {
			return dbtrPrvtIdIssr;
		}

		public void setDbtrPrvtIdIssr(String dbtrPrvtIdIssr) {
			this.dbtrPrvtIdIssr = dbtrPrvtIdIssr;
		}

		
	}

	public String getInstrId() {
		return instrId;
	}

	public void setInstrId(String instrId) {
		this.instrId = instrId;
	}

	public String getEndToEndId() {
		return endToEndId;
	}

	public void setEndToEndId(String endToEndId) {
		this.endToEndId = endToEndId;
	}

	public String getTxId() {
		return txId;
	}

	public void setTxId(String txId) {
		this.txId = txId;
	}

	public String getClrSysRef() {
		return clrSysRef;
	}

	public void setClrSysRef(String clrSysRef) {
		this.clrSysRef = clrSysRef;
	}

	public String getLclInstrm() {
		return lclInstrm;
	}

	public void setLclInstrm(String lclInstrm) {
		this.lclInstrm = lclInstrm;
	}

	public String getCtgyPurp() {
		return ctgyPurp;
	}

	public void setCtgyPurp(String ctgyPurp) {
		this.ctgyPurp = ctgyPurp;
	}

	
	
	public String getIntrBkSttlmAmtCcy() {
		return intrBkSttlmAmtCcy;
	}

	public void setIntrBkSttlmAmtCcy(String intrBkSttlmAmtCcy) {
		this.intrBkSttlmAmtCcy = intrBkSttlmAmtCcy;
	}

	

	public String getIntrBkSttlmDt() {
		return intrBkSttlmDt;
	}

	public void setIntrBkSttlmDt(String intrBkSttlmDt) {
		this.intrBkSttlmDt = intrBkSttlmDt;
	}

	public String getSttlmTmIndctn() {
		return sttlmTmIndctn;
	}

	public void setSttlmTmIndctn(String sttlmTmIndctn) {
		this.sttlmTmIndctn = sttlmTmIndctn;
	}

	public String getInstdAmt() {
		return instdAmt;
	}

	public void setInstdAmt(String instdAmt) {
		this.instdAmt = instdAmt;
	}

	public String getInstdAmtCcy() {
		return instdAmtCcy;
	}

	public void setInstdAmtCcy(String instdAmtCcy) {
		this.instdAmtCcy = instdAmtCcy;
	}

	public String getChrgBr() {
		return chrgBr;
	}

	public void setChrgBr(String chrgBr) {
		this.chrgBr = chrgBr;
	}

	public String getChrgAgentId() {
		return chrgAgentId;
	}

	public void setChrgAgentId(String chrgAgentId) {
		this.chrgAgentId = chrgAgentId;
	}

	public String getChrgAgentBic() {
		return chrgAgentBic;
	}

	public void setChrgAgentBic(String chrgAgentBic) {
		this.chrgAgentBic = chrgAgentBic;
	}

	public String getChrgCcy() {
		return chrgCcy;
	}

	public void setChrgCcy(String chrgCcy) {
		this.chrgCcy = chrgCcy;
	}

	public String getIntrBkSttlmAmt() {
		return intrBkSttlmAmt;
	}

	public void setIntrBkSttlmAmt(String intrBkSttlmAmt) {
		this.intrBkSttlmAmt = intrBkSttlmAmt;
	}

	public String getChrgAmount() {
		return chrgAmount;
	}

	public void setChrgAmount(String chrgAmount) {
		this.chrgAmount = chrgAmount;
	}

	public String getDbtrNm() {
		return dbtrNm;
	}

	public void setDbtrNm(String dbtrNm) {
		this.dbtrNm = dbtrNm;
	}

	public String getDbtrOrgIdAnyBIC() {
		return dbtrOrgIdAnyBIC;
	}

	public void setDbtrOrgIdAnyBIC(String dbtrOrgIdAnyBIC) {
		this.dbtrOrgIdAnyBIC = dbtrOrgIdAnyBIC;
	}

	public String getDbtrOrgIdOthrId() {
		return dbtrOrgIdOthrId;
	}

	public void setDbtrOrgIdOthrId(String dbtrOrgIdOthrId) {
		this.dbtrOrgIdOthrId = dbtrOrgIdOthrId;
	}

	public String getDbtrOrgIdOthrIdSchmeNm() {
		return dbtrOrgIdOthrIdSchmeNm;
	}

	public void setDbtrOrgIdOthrIdSchmeNm(String dbtrOrgIdOthrIdSchmeNm) {
		this.dbtrOrgIdOthrIdSchmeNm = dbtrOrgIdOthrIdSchmeNm;
	}

	public String getDbtrOrgIdOthrIssr() {
		return dbtrOrgIdOthrIssr;
	}

	public void setDbtrOrgIdOthrIssr(String dbtrOrgIdOthrIssr) {
		this.dbtrOrgIdOthrIssr = dbtrOrgIdOthrIssr;
	}

	public String getDbtrPrvtIdOthrId() {
		return dbtrPrvtIdOthrId;
	}

	public void setDbtrPrvtIdOthrId(String dbtrPrvtIdOthrId) {
		this.dbtrPrvtIdOthrId = dbtrPrvtIdOthrId;
	}

	public String getDbtrPrvtIdOthrIdSchmeNm() {
		return dbtrPrvtIdOthrIdSchmeNm;
	}

	public void setDbtrPrvtIdOthrIdSchmeNm(String dbtrPrvtIdOthrIdSchmeNm) {
		this.dbtrPrvtIdOthrIdSchmeNm = dbtrPrvtIdOthrIdSchmeNm;
	}

	public String getDbtrPrvtIdOthrIssr() {
		return dbtrPrvtIdOthrIssr;
	}

	public void setDbtrPrvtIdOthrIssr(String dbtrPrvtIdOthrIssr) {
		this.dbtrPrvtIdOthrIssr = dbtrPrvtIdOthrIssr;
	}

	public String getDbtrMobNb() {
		return dbtrMobNb;
	}

	public void setDbtrMobNb(String dbtrMobNb) {
		this.dbtrMobNb = dbtrMobNb;
	}

	public String getDbtrEmailAdr() {
		return dbtrEmailAdr;
	}

	public void setDbtrEmailAdr(String dbtrEmailAdr) {
		this.dbtrEmailAdr = dbtrEmailAdr;
	}

	public String getDbtrAcctId() {
		return dbtrAcctId;
	}

	public void setDbtrAcctId(String dbtrAcctId) {
		this.dbtrAcctId = dbtrAcctId;
	}

	public String getDbtrAccSchmeNm() {
		return dbtrAccSchmeNm;
	}

	public void setDbtrAccSchmeNm(String dbtrAccSchmeNm) {
		this.dbtrAccSchmeNm = dbtrAccSchmeNm;
	}

	public String getDbtrAgBICFI() {
		return dbtrAgBICFI;
	}

	public void setDbtrAgBICFI(String dbtrAgBICFI) {
		this.dbtrAgBICFI = dbtrAgBICFI;
	}

	public String getDbtrAgClrSysMmbId() {
		return dbtrAgClrSysMmbId;
	}

	public void setDbtrAgClrSysMmbId(String dbtrAgClrSysMmbId) {
		this.dbtrAgClrSysMmbId = dbtrAgClrSysMmbId;
	}

	public String getCdtrAgtBICFI() {
		return cdtrAgtBICFI;
	}

	public void setCdtrAgtBICFI(String cdtrAgtBICFI) {
		this.cdtrAgtBICFI = cdtrAgtBICFI;
	}

	public String getCdtrAgtClrSysMmbId() {
		return cdtrAgtClrSysMmbId;
	}

	public void setCdtrAgtClrSysMmbId(String cdtrAgtClrSysMmbId) {
		this.cdtrAgtClrSysMmbId = cdtrAgtClrSysMmbId;
	}

	public String getCdtrNm() {
		return cdtrNm;
	}

	public void setCdtrNm(String cdtrNm) {
		this.cdtrNm = cdtrNm;
	}

	public String getCdtrOrgIdAnyBIC() {
		return cdtrOrgIdAnyBIC;
	}

	public void setCdtrOrgIdAnyBIC(String cdtrOrgIdAnyBIC) {
		this.cdtrOrgIdAnyBIC = cdtrOrgIdAnyBIC;
	}

	public String getCdtrOrgIdOthrId() {
		return cdtrOrgIdOthrId;
	}

	public void setCdtrOrgIdOthrId(String cdtrOrgIdOthrId) {
		this.cdtrOrgIdOthrId = cdtrOrgIdOthrId;
	}

	public String getCdtrOrgIdOthrIdSchmeNm() {
		return cdtrOrgIdOthrIdSchmeNm;
	}

	public void setCdtrOrgIdOthrIdSchmeNm(String cdtrOrgIdOthrIdSchmeNm) {
		this.cdtrOrgIdOthrIdSchmeNm = cdtrOrgIdOthrIdSchmeNm;
	}

	public String getCdtrOrgIdOthrIssr() {
		return cdtrOrgIdOthrIssr;
	}

	public void setCdtrOrgIdOthrIssr(String cdtrOrgIdOthrIssr) {
		this.cdtrOrgIdOthrIssr = cdtrOrgIdOthrIssr;
	}

	public String getCdtrPrvtIdOthrId() {
		return cdtrPrvtIdOthrId;
	}

	public void setCdtrPrvtIdOthrId(String cdtrPrvtIdOthrId) {
		this.cdtrPrvtIdOthrId = cdtrPrvtIdOthrId;
	}

	public String getCdtrPrvtIdOthrIdSchmeNm() {
		return cdtrPrvtIdOthrIdSchmeNm;
	}

	public void setCdtrPrvtIdOthrIdSchmeNm(String cdtrPrvtIdOthrIdSchmeNm) {
		this.cdtrPrvtIdOthrIdSchmeNm = cdtrPrvtIdOthrIdSchmeNm;
	}

	public String getCdtrPrvtIdOthrIssr() {
		return cdtrPrvtIdOthrIssr;
	}

	public void setCdtrPrvtIdOthrIssr(String cdtrPrvtIdOthrIssr) {
		this.cdtrPrvtIdOthrIssr = cdtrPrvtIdOthrIssr;
	}

	public String getCdtrMobNb() {
		return cdtrMobNb;
	}

	public void setCdtrMobNb(String cdtrMobNb) {
		this.cdtrMobNb = cdtrMobNb;
	}

	public String getCdtrEmailAdr() {
		return cdtrEmailAdr;
	}

	public void setCdtrEmailAdr(String cdtrEmailAdr) {
		this.cdtrEmailAdr = cdtrEmailAdr;
	}

	public String getCdtrAcctId() {
		return cdtrAcctId;
	}

	public void setCdtrAcctId(String cdtrAcctId) {
		this.cdtrAcctId = cdtrAcctId;
	}

	public String getCdtrAcctSchmeNm() {
		return cdtrAcctSchmeNm;
	}

	public void setCdtrAcctSchmeNm(String cdtrAcctSchmeNm) {
		this.cdtrAcctSchmeNm = cdtrAcctSchmeNm;
	}

	public String getPurpCd() {
		return purpCd;
	}

	public void setPurpCd(String purpCd) {
		this.purpCd = purpCd;
	}

	public String getPurpPrtry() {
		return purpPrtry;
	}

	public void setPurpPrtry(String purpPrtry) {
		this.purpPrtry = purpPrtry;
	}

	public String getRmtInf() {
		return rmtInf;
	}

	public void setRmtInf(String rmtInf) {
		this.rmtInf = rmtInf;
	}

	

}
