package com.formssi.business.common.entity;

import java.util.Date;
import java.util.List;

public class SimVO_Pacs008 extends SimVOBase {
	
	private String msgId;
	
	private Date creDtTm;
	
	private String nbOfTxs;
	
	private String sttlmMtd;
	
	private String clrSys;
	
	private List<SimVO_Pacs008_CdtTrfTxInf> cdtTrfTxInfList;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public Date getCreDtTm() {
		return creDtTm;
	}

	public void setCreDtTm(Date creDtTm) {
		this.creDtTm = creDtTm;
	}

	public String getNbOfTxs() {
		return nbOfTxs;
	}

	public void setNbOfTxs(String nbOfTxs) {
		this.nbOfTxs = nbOfTxs;
	}

	public String getSttlmMtd() {
		return sttlmMtd;
	}

	public void setSttlmMtd(String sttlmMtd) {
		this.sttlmMtd = sttlmMtd;
	}

	public String getClrSys() {
		return clrSys;
	}

	public void setClrSys(String clrSys) {
		this.clrSys = clrSys;
	}

	public List<SimVO_Pacs008_CdtTrfTxInf> getCdtTrfTxInfList() {
		return cdtTrfTxInfList;
	}

	public void setCdtTrfTxInfList(List<SimVO_Pacs008_CdtTrfTxInf> cdtTrfTxInfList) {
		this.cdtTrfTxInfList = cdtTrfTxInfList;
	}
	
}
