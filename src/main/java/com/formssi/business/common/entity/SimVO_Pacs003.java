package com.formssi.business.common.entity;

import java.util.List;

public class SimVO_Pacs003 extends SimVOBase
{
	private String msgId;
	private String creDtTm;
	private String sttlmMtd;
	private String prtry;
	private String clrSys;

	private List<SimVO_Pacs003_DrctDbtTxInf> drctDbtTxInfList;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getCreDtTm() {
		return creDtTm;
	}

	public void setCreDtTm(String creDtTm) {
		this.creDtTm = creDtTm;
	}

	public String getSttlmMtd() {
		return sttlmMtd;
	}

	public void setSttlmMtd(String sttlmMtd) {
		this.sttlmMtd = sttlmMtd;
	}

	public String getPrtry() {
		return prtry;
	}

	public void setPrtry(String prtry) {
		this.prtry = prtry;
	}

	public String getClrSys() {
		return clrSys;
	}

	public void setClrSys(String clrSys) {
		this.clrSys = clrSys;
	}

	public List<SimVO_Pacs003_DrctDbtTxInf> getDrctDbtTxInfList() {
		return drctDbtTxInfList;
	}

	public void setDrctDbtTxInfList(List<SimVO_Pacs003_DrctDbtTxInf> drctDbtTxInfList) {
		this.drctDbtTxInfList = drctDbtTxInfList;
	}

	
	

}
