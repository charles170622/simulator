package com.formssi.business.iclfps.pacs008;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.forms.ffp.adaptor.jaxb.iclfps.fps_envelope_01.ISO20022BusinessDataV01;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.BranchAndFinancialInstitutionIdentification51;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.CashAccount241;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.CreditTransferTransaction251;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.Document;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.GenericOrganisationIdentification11;
import com.formssi.business.common.entity.SimTxBase;
import com.formssi.business.common.entity.SimVO_Pacs008;
import com.formssi.business.common.entity.SimVO_Pacs008_CdtTrfTxInf;
import com.formssi.msg.SimAdaptorMgr;
import com.formssi.msg.iclfps.SimSendMessageResp;

@Component("ICL.pacs.008.001.06")
@Scope("prototype")
public class SimTxPacs008 extends SimTxBase
{
	private Logger _logger = LoggerFactory.getLogger(SimTxPacs008.class);
	
	public void perform() throws Exception{
		
		if ("ICL.pacs.008.001.06".equals(this.serviceName)){
			SimVO_Pacs008 loc_vo = (SimVO_Pacs008) txVo;
			SimMsgPacs008_Pacs002 msg = new SimMsgPacs008_Pacs002(loc_vo, 0);
			
			SimSendMessageResp resp = SimAdaptorMgr.getInstance().execute(msg);
		}
	}

	public boolean validate(){
		return true;
	}

	public void parseISO20022BizData(ISO20022BusinessDataV01 bizData) throws Exception{

		if ("ICL.pacs.008.001.06".equals(this.serviceName)) {
			txVo = new SimVO_Pacs008();
			SimVO_Pacs008 loc_vo = (SimVO_Pacs008) txVo;

			parseISO20022BizDataHead(bizData);

			Document doc = (Document) bizData.getContent().get(1).getValue();
			
			loc_vo.setMsgId(doc.getFIToFICstmrCdtTrf().getGrpHdr().getMsgId());
			loc_vo.setCreDtTm(doc.getFIToFICstmrCdtTrf().getGrpHdr().getCreDtTm().toGregorianCalendar().getTime());
			String numberOfTx = doc.getFIToFICstmrCdtTrf().getGrpHdr().getNbOfTxs();
			loc_vo.setNbOfTxs(numberOfTx);
			loc_vo.setSttlmMtd(doc.getFIToFICstmrCdtTrf().getGrpHdr().getSttlmInf().getSttlmMtd().value());
			loc_vo.setClrSys(doc.getFIToFICstmrCdtTrf().getGrpHdr().getSttlmInf().getClrSys().getPrtry().value());
			List<CreditTransferTransaction251> list = doc.getFIToFICstmrCdtTrf().getCdtTrfTxInf();
			List<SimVO_Pacs008_CdtTrfTxInf> pacs008TxInf = new ArrayList<SimVO_Pacs008_CdtTrfTxInf>();
			loc_vo.setCdtTrfTxInfList(pacs008TxInf);

			for (CreditTransferTransaction251 ct251 : list) {
				SimVO_Pacs008_CdtTrfTxInf cdtTxInfo = new SimVO_Pacs008_CdtTrfTxInf();
				pacs008TxInf.add(cdtTxInfo);
				cdtTxInfo.setInstrId(ct251.getPmtId().getInstrId());
				cdtTxInfo.setEndToEndId(ct251.getPmtId().getEndToEndId());
				cdtTxInfo.setTxId(ct251.getPmtId().getTxId());
				cdtTxInfo.setClrSysRef(ct251.getPmtId().getClrSysRef());

				// payment type information
				if (ct251.getPmtTpInf() != null && ct251.getPmtTpInf().getLclInstrm() != null) {
					cdtTxInfo.setLclInstrm(ct251.getPmtTpInf().getLclInstrm().getPrtry().value()); 
					if (ct251.getPmtTpInf().getCtgyPurp() != null) {
						cdtTxInfo.setCtgyPurp(ct251.getPmtTpInf().getCtgyPurp().getPrtry().value());
					}
				}
				BigDecimal amt = ct251.getIntrBkSttlmAmt().getValue();
				cdtTxInfo.setIntrBkSttlmAmt(String.valueOf(amt));
				cdtTxInfo.setIntrBkSttlmAmtCcy(ct251.getIntrBkSttlmAmt().getCcy().value());

				if (ct251.getIntrBkSttlmDt() != null) {
					Date sttlmDt = ct251.getIntrBkSttlmDt().toGregorianCalendar().getTime();
					cdtTxInfo.setIntrBkSttlmDt(new SimpleDateFormat().format(sttlmDt));
				}
				if (ct251.getSttlmTmIndctn() != null && ct251.getSttlmTmIndctn().getCdtDtTm() != null) {
					Date tmIndctn = ct251.getSttlmTmIndctn().getCdtDtTm().toGregorianCalendar().getTime();
					cdtTxInfo.setSttlmTmIndctn(new SimpleDateFormat().format(tmIndctn));
				}
				if (ct251.getInstdAmt() != null) {
					cdtTxInfo.setInstdAmt(String.valueOf(ct251.getInstdAmt().getValue()));
					cdtTxInfo.setInstdAmtCcy(ct251.getInstdAmt().getCcy().value());
				}

				// charge info.
				cdtTxInfo.setChrgBr(ct251.getChrgBr().value());
				if(ct251.getChrgsInf() != null)
				{
					cdtTxInfo.setChrgAmount(String.valueOf(ct251.getChrgsInf().getAmt().getValue()));
					cdtTxInfo.setChrgCcy(ct251.getChrgsInf().getAmt().getCcy().value());
					cdtTxInfo.setChrgAgentBic(ct251.getChrgsInf().getAgt().getFinInstnId().getBICFI());
					cdtTxInfo.setChrgAgentBic(ct251.getChrgsInf().getAgt().getFinInstnId().getClrSysMmbId().getMmbId());
				}

				// Debtor
				cdtTxInfo.setDbtrNm(ct251.getDbtr().getNm());
				if (ct251.getDbtr().getId() != null) {
					if (ct251.getDbtr().getId().getOrgId() != null) {
						cdtTxInfo.setDbtrOrgIdAnyBIC(ct251.getDbtr().getId().getOrgId().getAnyBIC());
						GenericOrganisationIdentification11 other = ct251.getDbtr().getId().getOrgId().getOthr();
						if (other != null) // 0..*,maybe a collection
						{
							cdtTxInfo.setDbtrOrgIdOthrId(other.getId());
							cdtTxInfo.setDbtrOrgIdOthrIdSchmeNm(other.getSchmeNm().getCd().value());
							cdtTxInfo.setDbtrOrgIdOthrIssr(other.getIssr());
						}
					}
					else if(ct251.getDbtr().getId().getPrvtId() != null)
					{
						
						cdtTxInfo.setDbtrPrvtIdOthrId(ct251.getDbtr().getId().getPrvtId().getOthr().getId());
						cdtTxInfo.setDbtrPrvtIdOthrIdSchmeNm(ct251.getDbtr().getId().getPrvtId().getOthr().getSchmeNm().getCd().value());
						cdtTxInfo.setDbtrPrvtIdOthrIssr(ct251.getDbtr().getId().getPrvtId().getOthr().getIssr());
					}
				}

				cdtTxInfo.setDbtrMobNb(
						ct251.getDbtr().getCtctDtls() != null ? ct251.getDbtr().getCtctDtls().getMobNb() : null);
				cdtTxInfo.setDbtrEmailAdr(
						ct251.getDbtr().getCtctDtls() != null ? ct251.getDbtr().getCtctDtls().getEmailAdr() : null);

				// Debtor Account and Debtor Agent
				CashAccount241 dbtAcc = ct251.getDbtrAcct();
				BranchAndFinancialInstitutionIdentification51 dbtAgt = ct251.getDbtrAgt();
				if (dbtAcc != null) {
					cdtTxInfo.setDbtrAcctId(dbtAcc.getId().getOthr().getId());
					cdtTxInfo.setDbtrAccSchmeNm(dbtAcc.getId().getOthr().getSchmeNm() != null
							? dbtAcc.getId().getOthr().getSchmeNm().getPrtry().value() : null);
				}

				if (dbtAgt != null) {
					cdtTxInfo.setDbtrAgBICFI(dbtAgt.getFinInstnId().getBICFI());
					cdtTxInfo.setDbtrAgClrSysMmbId(dbtAgt.getFinInstnId().getClrSysMmbId() != null
							? dbtAgt.getFinInstnId().getClrSysMmbId().getMmbId() : null);
				}

				// Creditor Agent
				BranchAndFinancialInstitutionIdentification51 cdtrAgt = ct251.getCdtrAgt();
				if (cdtrAgt != null) {
					cdtTxInfo.setCdtrAgtBICFI(cdtrAgt.getFinInstnId().getBICFI());
					cdtTxInfo.setCdtrAgtClrSysMmbId(cdtrAgt.getFinInstnId().getClrSysMmbId() != null
							? cdtrAgt.getFinInstnId().getClrSysMmbId().getMmbId() : null);
				}
				// Creditor
				cdtTxInfo.setCdtrNm(ct251.getCdtr().getNm());
				if (ct251.getCdtr().getId() != null) {
					if (ct251.getCdtr().getId().getOrgId() != null) {
						cdtTxInfo.setCdtrOrgIdAnyBIC(ct251.getCdtr().getId().getOrgId().getAnyBIC());
						GenericOrganisationIdentification11 other = ct251.getCdtr().getId().getOrgId().getOthr();
						if (other != null) // 0..*,maybe a collection
						{
							cdtTxInfo.setCdtrOrgIdOthrId(other.getId());
							cdtTxInfo.setCdtrOrgIdOthrIdSchmeNm(other.getSchmeNm().getCd().value());
							cdtTxInfo.setCdtrOrgIdOthrIssr(other.getIssr());
						}
					}
					else if(ct251.getCdtr().getId().getPrvtId() != null)
					{
						cdtTxInfo.setCdtrPrvtIdOthrId(ct251.getCdtr().getId().getPrvtId().getOthr().getId());
						cdtTxInfo.setCdtrPrvtIdOthrIdSchmeNm(ct251.getCdtr().getId().getPrvtId().getOthr().getSchmeNm().getCd().value());
						cdtTxInfo.setCdtrPrvtIdOthrIssr(ct251.getCdtr().getId().getPrvtId().getOthr().getIssr());
					}
				}

				cdtTxInfo.setCdtrMobNb(
						ct251.getCdtr().getCtctDtls() != null ? ct251.getCdtr().getCtctDtls().getMobNb() : null);
				cdtTxInfo.setCdtrEmailAdr(
						ct251.getCdtr().getCtctDtls() != null ? ct251.getCdtr().getCtctDtls().getEmailAdr() : null);

				// Creditor Account
				CashAccount241 cbtrAcc = ct251.getCdtrAcct();
				if (cbtrAcc != null) {
					cdtTxInfo.setCdtrAcctId(cbtrAcc.getId().getOthr().getId());
					cdtTxInfo.setCdtrAcctSchmeNm(cbtrAcc.getId().getOthr().getSchmeNm() != null
							? cbtrAcc.getId().getOthr().getSchmeNm().getPrtry().value() : null);
				}

				// purpose
				if (ct251.getPurp() != null) {
					cdtTxInfo.setPurpCd(ct251.getPurp().getCd());
					cdtTxInfo.setPurpPrtry(ct251.getPurp().getPrtry());
				}

				// Remittance information
				if (ct251.getRmtInf() != null) {
					cdtTxInfo.setRmtInf(ct251.getRmtInf().getUstrd());
				}
			}
		}
	}
}
