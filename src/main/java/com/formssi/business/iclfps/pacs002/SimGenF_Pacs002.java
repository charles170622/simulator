package com.formssi.business.iclfps.pacs002;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.forms.ffp.adaptor.jaxb.iclfps.fps_envelope_01.ISO20022BusinessDataV01;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_002_001_08.Document;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_002_001_08.FIToFIPaymentStatusReportV08;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_002_001_08.GroupHeader531;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_002_001_08.OriginalGroupHeader71;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_002_001_08.PaymentTransaction801;
import com.formssi.bean.SimCTOReceiveBean;
import com.formssi.business.common.entity.SimTxBase;
import com.formssi.business.common.entity.SimVO_Pacs002;
import com.formssi.business.common.entity.SimVO_Pacs002_TxInfAndSts;
import com.formssi.msg.SimLocalFileConvertor;
import com.formssi.service.SimBridgeConfigSvc;
import com.formssi.utils.SimFileNameUtils;
import com.formssi.utils.SimStringUtils;

@Component("ICL.pacs.002.001.08")
@Scope("prototype")
public class SimGenF_Pacs002 extends SimTxBase{
	
	@Autowired
	private SimBridgeConfigSvc bridgeConfig;

	public void perform() throws Exception{
		
		if ("ICL.pacs.002.001.08".equals(this.serviceName)){
			
			SimVO_Pacs002 loc_pacs002 = (SimVO_Pacs002) txVo;
			
			String txId = loc_pacs002.getTxInfList().get(0).getOrgnlTxId();
			List<String> txIdList = bridgeConfig.getTxIdList();
			if(txIdList!=null && txIdList.size()>0 ){
				if(txIdList.contains(txId)){
					
					SimCTOReceiveBean bean = new SimCTOReceiveBean();
					String msgContent = loc_pacs002.getIclfpsWrapper().getMessage().getText();
					SimVO_Pacs002_TxInfAndSts txInfo= loc_pacs002.getTxInfList().get(0);
					
					bean.setTransactionID(txInfo.getOrgnlTxId());
					bean.setTransactionType(loc_pacs002.getBizSvc());
					bean.setMessageID(loc_pacs002.getMsgId());
					bean.setMessageType(loc_pacs002.getMsgDefIdr());
					bean.setStatus(txInfo.getTxSts());
					bean.setCurrency(txInfo.getSettlementCurrency());
					String amount = null;
					if(txInfo.getSettlementAmount() != null){
						amount = String.valueOf(txInfo.getSettlementAmount());
					}
					bean.setAmount(amount);
					bean.setRejectCode(txInfo.getTxStsRsnCode());
					bean.getFileInfoMap().put(SimFileNameUtils.getFileName(loc_pacs002.getBizSvc(), txId), msgContent);
					
					Integer messageCount = bridgeConfig.getMessageCount();
					messageCount = messageCount == null ? 0 : messageCount;
					messageCount++;
					bridgeConfig.setMessageCount(messageCount);
					bridgeConfig.getListenerCommonQueue().put(bean);
					
					
					if(messageCount == txIdList.size()){
						messageCount = null;
						SimLocalFileConvertor.getInstance().commonWrite2Local(bridgeConfig);
					}
				}
			}
		}
	}

	

	public boolean validate(){
		return true;
	}

	public void parseISO20022BizData(ISO20022BusinessDataV01 bizData) throws Exception{
		
		if ("ICL.pacs.002.001.08".equals(this.serviceName)){
			
			txVo = new SimVO_Pacs002();
			parseISO20022BizDataHead(bizData);

			SimVO_Pacs002 loc_vo = (SimVO_Pacs002) txVo;
			// document get(1) not get(0)
			Document doc = (Document) bizData.getContent().get(1).getValue();

			FIToFIPaymentStatusReportV08 fiToFIPmtStsRpt = doc.getFIToFIPmtStsRpt();
			GroupHeader531 grpHdr = fiToFIPmtStsRpt.getGrpHdr();
			loc_vo.setMsgId(grpHdr.getMsgId());
			loc_vo.setCreDtTm(grpHdr.getCreDtTm().toString());
			// FFPDateUtils.convertStringToDate(grpHdr.getCreDtTm().toString(),
			// FFPDateUtils.HK_TIMESTAMP_FORMAT1));

			OriginalGroupHeader71 orgnlGrpInfAndSts = fiToFIPmtStsRpt.getOrgnlGrpInfAndSts();
			loc_vo.setOrgnlMsgId(orgnlGrpInfAndSts.getOrgnlMsgId());
			loc_vo.setOrgnlMsgNmId(orgnlGrpInfAndSts.getOrgnlMsgNmId());
			
			if (orgnlGrpInfAndSts.getGrpSts() != null){
				loc_vo.setGrpSts(orgnlGrpInfAndSts.getGrpSts().value());
			}
			if (orgnlGrpInfAndSts.getStsRsnInf() != null){
				
				if (orgnlGrpInfAndSts.getStsRsnInf().getRsn() != null)
				{
					loc_vo.setGrpStsRsnCode(orgnlGrpInfAndSts.getStsRsnInf().getRsn().getPrtry());
				}
				List<String> addtlInf = orgnlGrpInfAndSts.getStsRsnInf().getAddtlInf();
				if (addtlInf != null)
				{
					loc_vo.setGrpStsAddtlInfList(addtlInf);
				}
			}
			List<SimVO_Pacs002_TxInfAndSts> txInfList = new ArrayList<SimVO_Pacs002_TxInfAndSts>();
			List<PaymentTransaction801> txInfAndSts = fiToFIPmtStsRpt.getTxInfAndSts();
			for (PaymentTransaction801 pt801 : txInfAndSts){
				
				SimVO_Pacs002_TxInfAndSts txInfo = new SimVO_Pacs002_TxInfAndSts();
				txInfo.setOrgnlEndToEndId(pt801.getOrgnlEndToEndId());
				txInfo.setOrgnlTxId(pt801.getOrgnlTxId());
				txInfo.setTxSts(pt801.getTxSts().value());
				
				if(pt801.getStsRsnInf() != null){
					
					if(pt801.getStsRsnInf().getRsn() != null)
						txInfo.setTxStsRsnCode(pt801.getStsRsnInf().getRsn().getPrtry());
					if (null != pt801.getStsRsnInf().getAddtlInf())
						txInfo.setTxStsAddtlInfList(pt801.getStsRsnInf().getAddtlInf());
				}
				
				if(pt801.getAccptncDtTm() != null)
					txInfo.setAccptncDtTm(pt801.getAccptncDtTm().toGregorianCalendar().getTime());

				if (!(SimStringUtils.isEmptyOrNull(pt801.getClrSysRef()))){
					txInfo.setClrSysRef(pt801.getClrSysRef());
				}

				if(pt801.getOrgnlTxRef() != null){
					
					if(pt801.getOrgnlTxRef().getIntrBkSttlmAmt() != null){
						txInfo.setSettlementCurrency(pt801.getOrgnlTxRef().getIntrBkSttlmAmt().getCcy().value());
						txInfo.setSettlementAmount(pt801.getOrgnlTxRef().getIntrBkSttlmAmt().getValue());
					}
					if(pt801.getOrgnlTxRef().getIntrBkSttlmDt() != null)
						txInfo.setSettlementDate(pt801.getOrgnlTxRef().getIntrBkSttlmDt().toGregorianCalendar().getTime());
				}

				txInfList.add(txInfo);

			}
			loc_vo.setTxInfList(txInfList);
		}
	}
}
