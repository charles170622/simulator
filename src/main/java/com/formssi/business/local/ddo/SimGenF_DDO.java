package com.formssi.business.local.ddo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.cglib.beans.BeanMap;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.formssi.business.common.entity.SimTxBase;
import com.formssi.business.common.entity.SimVO_Pacs003;
import com.formssi.business.common.entity.SimVO_Pacs003_DrctDbtTxInf;
import com.formssi.msg.SimLocalFileConvertor;
import com.formssi.utils.SimStringUtils;

@Component("LOCAL.DDO")
@Scope("prototype")
public class SimGenF_DDO extends SimTxBase {

	@Override
	public void perform() throws Exception {
		SimVO_Pacs003 vo = (SimVO_Pacs003)txVo;
		SimMsg_DDO msg = new SimMsg_DDO(vo);
		SimLocalFileConvertor.getInstance().convertToSendXmlFile(msg, this.dirName);
	}

	@Override
	public boolean validate() throws Exception {
		return true;
	}

	@Override
	public void parseLocalFileData(Map<String, Object> dataMap, String dirName) throws Exception {
		if(dataMap.isEmpty()){
			return ;
		}
		
		if ("LOCAL.DDO".equals(this.serviceName)){
			
			txVo = new SimVO_Pacs003();
			SimVO_Pacs003 vo = (SimVO_Pacs003)txVo;
			
			this.dirName = dirName;
			
			List<SimVO_Pacs003_DrctDbtTxInf> drctDbtTxInfList = new ArrayList<>();
			SimVO_Pacs003_DrctDbtTxInf drctDbtTxInf = new SimVO_Pacs003_DrctDbtTxInf();
			drctDbtTxInf = mapToBean(dataMap, drctDbtTxInf);
			
			if(!SimStringUtils.isEmptyOrNull((String)dataMap.get("bizSvc")))
				txVo.setBizSvc((String)dataMap.get("bizSvc"));
			
			drctDbtTxInfList.add(drctDbtTxInf);
			vo.setDrctDbtTxInfList(drctDbtTxInfList);
		}
	}
	
	/** 
	 * 将map装换为javabean对象 
	 * @param map 
	 * @param bean 
	 * @return 
	 */
	private static <T> T mapToBean(Map<String, Object> map,T bean) { 
	  BeanMap beanMap = BeanMap.create(bean); 
	  beanMap.putAll(map); 
	  return bean; 
	} 
	
}
