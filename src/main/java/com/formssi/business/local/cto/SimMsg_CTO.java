package com.formssi.business.local.cto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBElement;

import org.springframework.web.util.HtmlUtils;

import com.forms.ffp.adaptor.define.FFPJaxbConstants;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.AccountIdentification4Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.AccountSchemeName1Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.BranchAndFinancialInstitutionIdentification51;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.CashAccount241;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.CategoryPurpose1Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.ChargeBearerType1Code1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.ClearingSystemIdentification3Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.ClearingSystemMemberIdentification21;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.ContactDetails21;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.CreditTransferTransaction251;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.Document;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FIToFICustomerCreditTransferV06;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSAccountTypeCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSAccountVerificationOptionCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSCategoryPurposeCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSClearingSystemCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSCurrencyCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FPSCustomerCode;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.FinancialInstitutionIdentification81;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.GenericAccountIdentification11;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.GenericOrganisationIdentification11;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.GenericPersonIdentification11;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.GroupHeader701;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.LocalInstrument2Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.ObjectFactory;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.OrganisationIdentification81;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.OrganisationIdentificationSchemeName1Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.Party11Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.PartyIdentification431;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.PaymentIdentification31;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.PaymentTypeInformation211;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.PersonIdentification51;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.PersonIdentificationSchemeName1Choice1;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.Restricted15Digit2DecimalCurrencyAndAmount;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.SettlementInstruction41;
import com.forms.ffp.adaptor.jaxb.iclfps.pacs_008_001_06.SettlementMethod1Code1;
import com.formssi.business.common.entity.SimVO_Pacs008;
import com.formssi.business.common.entity.SimVO_Pacs008_CdtTrfTxInf;
import com.formssi.defines.SimConstants;
import com.formssi.defines.SimConstantsSvcCd;
import com.formssi.msg.iclfps.SimMsgBaseHkiclMessage;
import com.formssi.utils.FFPValidateUtils;
import com.formssi.utils.SimStringUtils;
import com.formssi.utils.SimXMLUtils;

public class SimMsg_CTO extends SimMsgBaseHkiclMessage {

	private SimVO_Pacs008 txJb = null;

	private ObjectFactory _objFactory = new ObjectFactory();

	public SimMsg_CTO(SimVO_Pacs008 txJb){
		
		super();
		this.txJb = txJb;
		this.msgTypeName = FFPJaxbConstants.JAXB_MSG_TYPE_PACS_008;
		this.sendType = SimConstants.SEND_TYPE_REQ;
		//TODO get it from file
		if(SimConstantsSvcCd.ICLFPS_SERVICECODE_PAYC01.equalsIgnoreCase(txJb.getBizSvc())){
			this.msgBizSvc = SimConstantsSvcCd.ICLFPS_SERVICECODE_PAYC01;
			this.priority = SimConstants.MQ_LEVEL_PRIORITY_HIGH;
		}else if(SimConstantsSvcCd.ICLFPS_SERVICECODE_PAYC02.equalsIgnoreCase(txJb.getBizSvc())){
			this.msgBizSvc = SimConstantsSvcCd.ICLFPS_SERVICECODE_PAYC02;
			this.priority = SimConstants.MQ_LEVEL_PRIORITY_MEDIUM;
		}else{
			
		}
		
		this.txId = txJb.getCdtTrfTxInfList().get(0).getTxId();
	}

	public JAXBElement<?> marshalMsgBizDataDocument()
	{
		Document loc_doc = createDocument();
		return (new ObjectFactory()).createDocument(loc_doc);
	}

	private Document createDocument(){
		
		Document doc = this._objFactory.createDocument();
		
		FIToFICustomerCreditTransferV06 creditTransferV06 = this._objFactory.createFIToFICustomerCreditTransferV06();
		
		//doc head
		GroupHeader701 groupHeader701 = this._objFactory.createGroupHeader701();
		groupHeader701.setMsgId(txJb.getMsgId());
		groupHeader701.setCreDtTm(SimXMLUtils.toGregorianDtType1(this.getCreDt()));
		groupHeader701.setNbOfTxs(txJb.getNbOfTxs());
		
		SettlementInstruction41 settlementInstruction41 = this._objFactory.createSettlementInstruction41();
		ClearingSystemIdentification3Choice1 clearingSystemIdentification3Choice1 = this._objFactory.createClearingSystemIdentification3Choice1();
		clearingSystemIdentification3Choice1.setPrtry(FPSClearingSystemCode.FPS);
		settlementInstruction41.setClrSys(clearingSystemIdentification3Choice1);
		settlementInstruction41.setSttlmMtd(SettlementMethod1Code1.CLRG);
		groupHeader701.setSttlmInf(settlementInstruction41);
		creditTransferV06.setGrpHdr(groupHeader701);
		
		//doc body
		creditTransferV06.getCdtTrfTxInf().add(createCreditTransferTransaction251());
		doc.setFIToFICstmrCdtTrf(creditTransferV06);
		return doc;
	}

	private CreditTransferTransaction251 createCreditTransferTransaction251(){
		
		CreditTransferTransaction251 transaction251 = this._objFactory.createCreditTransferTransaction251();
		SimVO_Pacs008_CdtTrfTxInf cdtTrfTxInf = txJb.getCdtTrfTxInfList().get(0);
		
		PaymentIdentification31 paymentIdentification31 = this._objFactory.createPaymentIdentification31();
		paymentIdentification31.setTxId(cdtTrfTxInf.getTxId());
		paymentIdentification31.setEndToEndId(cdtTrfTxInf.getEndToEndId());
		paymentIdentification31.setClrSysRef(cdtTrfTxInf.getClrSysRef());
		paymentIdentification31.setInstrId(cdtTrfTxInf.getInstrId());
		transaction251.setPmtId(paymentIdentification31);
		
		PaymentTypeInformation211  paymentTypeInformation211= this._objFactory.createPaymentTypeInformation211();
		LocalInstrument2Choice1 instrument2Choice1 = this._objFactory.createLocalInstrument2Choice1();
		if(!SimStringUtils.isEmptyOrNull(cdtTrfTxInf.getLclInstrm()))
			instrument2Choice1.setPrtry(FPSAccountVerificationOptionCode.valueOf(cdtTrfTxInf.getLclInstrm()));
		paymentTypeInformation211.setLclInstrm(instrument2Choice1);
		
		CategoryPurpose1Choice1 categoryPurpose1Choice1 = this._objFactory.createCategoryPurpose1Choice1();
		if(!SimStringUtils.isEmptyOrNull(cdtTrfTxInf.getCtgyPurp()))
			categoryPurpose1Choice1.setPrtry(FPSCategoryPurposeCode.valueOf(cdtTrfTxInf.getCtgyPurp()));
		paymentTypeInformation211.setCtgyPurp(categoryPurpose1Choice1);
		
		transaction251.setPmtTpInf(paymentTypeInformation211);
		
		transaction251.setIntrBkSttlmAmt(createRestricted15Digit2DecimalCurrencyAndAmount(cdtTrfTxInf.getIntrBkSttlmAmtCcy(), cdtTrfTxInf.getIntrBkSttlmAmt()));
		String intrBkSttlmDt = cdtTrfTxInf.getIntrBkSttlmDt();
		if(!SimStringUtils.isEmptyOrNull(intrBkSttlmDt)){
			Date d = null;
			try {
				d = new SimpleDateFormat("yyyy-MM-dd").parse(intrBkSttlmDt);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			transaction251.setIntrBkSttlmDt(SimXMLUtils.toGregorianDtNoTs(d));
		}
		transaction251.setChrgBr(ChargeBearerType1Code1.SLEV);
		
		//dbtr
		transaction251.setDbtr(createPartyIdentification431(
				cdtTrfTxInf.getDbtrAccSchmeNm(), cdtTrfTxInf.getDbtrOrgIdAnyBIC(), cdtTrfTxInf.getDbtrOrgIdOthrId(), cdtTrfTxInf.getDbtrOrgIdOthrIdSchmeNm(), cdtTrfTxInf.getDbtrOrgIdOthrIssr(),
				cdtTrfTxInf.getDbtrPrvtIdOthrId(), cdtTrfTxInf.getDbtrPrvtIdOthrIdSchmeNm(), cdtTrfTxInf.getDbtrPrvtIdOthrIssr(), cdtTrfTxInf.getDbtrMobNb(), cdtTrfTxInf.getDbtrEmailAdr()
				));
		
		transaction251.setDbtrAcct(createCashAccount241(cdtTrfTxInf.getDbtrAcctId(), cdtTrfTxInf.getDbtrAccSchmeNm()));
		transaction251.setDbtrAgt(createBranchAndFinancialInstitutionIdentification51(cdtTrfTxInf.getDbtrAgClrSysMmbId(), cdtTrfTxInf.getDbtrAgBICFI()));
		
		//cdtr
		transaction251.setCdtr(createPartyIdentification431(
				cdtTrfTxInf.getCdtrAcctSchmeNm(), cdtTrfTxInf.getCdtrOrgIdAnyBIC(), cdtTrfTxInf.getCdtrOrgIdOthrId(), cdtTrfTxInf.getCdtrOrgIdOthrIdSchmeNm(), cdtTrfTxInf.getCdtrOrgIdOthrIssr(),
				cdtTrfTxInf.getCdtrPrvtIdOthrId(), cdtTrfTxInf.getCdtrPrvtIdOthrIdSchmeNm(), cdtTrfTxInf.getCdtrPrvtIdOthrIssr(), cdtTrfTxInf.getCdtrMobNb(), cdtTrfTxInf.getCdtrEmailAdr()
				));
		transaction251.setCdtrAcct(createCashAccount241(cdtTrfTxInf.getCdtrAcctId(), cdtTrfTxInf.getCdtrAcctSchmeNm()));
		transaction251.setCdtrAgt(createBranchAndFinancialInstitutionIdentification51(cdtTrfTxInf.getCdtrAgtClrSysMmbId(), cdtTrfTxInf.getCdtrAgtBICFI()));
		
		return transaction251;
	}
	
	private BranchAndFinancialInstitutionIdentification51 createBranchAndFinancialInstitutionIdentification51(String mmbId, String mmbBic){
		
		BranchAndFinancialInstitutionIdentification51 id = this._objFactory.createBranchAndFinancialInstitutionIdentification51();
		ClearingSystemMemberIdentification21 clrSysMmbId = this._objFactory.createClearingSystemMemberIdentification21();
		clrSysMmbId.setMmbId(mmbId);

		FinancialInstitutionIdentification81 finInstnId = this._objFactory.createFinancialInstitutionIdentification81();
		finInstnId.setBICFI(mmbBic);
		finInstnId.setClrSysMmbId(clrSysMmbId);

		id.setFinInstnId(finInstnId);

		return id;
	}
	
	private CashAccount241 createCashAccount241(String accNum, String accNumType)
	{
		CashAccount241 cashAcct = this._objFactory.createCashAccount241();

		AccountSchemeName1Choice1 schmeNm = this._objFactory.createAccountSchemeName1Choice1();
		if(!SimStringUtils.isEmptyOrNull(accNumType))
			schmeNm.setPrtry(FPSAccountTypeCode.fromValue(accNumType));

		GenericAccountIdentification11 othr = this._objFactory.createGenericAccountIdentification11();
		othr.setId(accNum);
		othr.setSchmeNm(schmeNm);

		AccountIdentification4Choice1 id = this._objFactory.createAccountIdentification4Choice1();
		id.setOthr(othr);
		cashAcct.setId(id);

		return cashAcct;
	}
	
	
	private Restricted15Digit2DecimalCurrencyAndAmount createRestricted15Digit2DecimalCurrencyAndAmount(String currency, String amount){
		String loc_currency = SimStringUtils.getStringValue(currency);
		if (FFPValidateUtils.isNullObject(new Object[] { amount }).booleanValue()){
			return null;
		}
		Restricted15Digit2DecimalCurrencyAndAmount currencyAndAmount = this._objFactory.createRestricted15Digit2DecimalCurrencyAndAmount();
		if(!SimStringUtils.isEmptyOrNull(loc_currency))
			currencyAndAmount.setCcy(FPSCurrencyCode.fromValue(loc_currency));
		currencyAndAmount.setValue(new BigDecimal(amount));

		return currencyAndAmount;
		
	}

	
	private PartyIdentification431 createPartyIdentification431(
			String acctName, String OrgIdAnyBIC, String OrgIdOthrId, String OrgIdOthrIdSchmeNm, String OrgIdOthrIssr,
			String PrvtIdOthrId, String PrvtIdOthrIdSchmeNm, String PrvtIdOthrIssr, String phoneNo, String emailAddr)
	{
		PartyIdentification431 pi431 = this._objFactory.createPartyIdentification431();
		pi431.setNm(acctName != null ? HtmlUtils.htmlUnescape(acctName) : null);

		if (!SimStringUtils.isEmptyOrNull(PrvtIdOthrId)){
			
			Party11Choice1 id = this._objFactory.createParty11Choice1();
			PersonIdentification51 prvtId = this._objFactory.createPersonIdentification51();
			GenericPersonIdentification11 othr = this._objFactory.createGenericPersonIdentification11();
			PersonIdentificationSchemeName1Choice1 schmeNm = this._objFactory.createPersonIdentificationSchemeName1Choice1();
			schmeNm.setCd(FPSCustomerCode.fromValue(PrvtIdOthrIdSchmeNm));
			othr.setId(PrvtIdOthrId);
			othr.setSchmeNm(schmeNm);
			prvtId.setOthr(othr);
			id.setPrvtId(prvtId);
			pi431.setId(id);
			
		} else if (!SimStringUtils.isEmptyOrNull(OrgIdOthrId)){
			
			Party11Choice1 id = this._objFactory.createParty11Choice1();
			OrganisationIdentification81 orgId = this._objFactory.createOrganisationIdentification81();
			GenericOrganisationIdentification11 othr = this._objFactory.createGenericOrganisationIdentification11();
			OrganisationIdentificationSchemeName1Choice1 schmeNm = this._objFactory.createOrganisationIdentificationSchemeName1Choice1();
			schmeNm.setCd(FPSCustomerCode.fromValue(OrgIdOthrIdSchmeNm));
			othr.setSchmeNm(schmeNm);
			othr.setId(OrgIdOthrId);
			othr.setIssr(OrgIdOthrIssr);
			orgId.setOthr(othr);
			orgId.setAnyBIC(OrgIdAnyBIC);
			id.setOrgId(orgId);
			pi431.setId(id);
		}

		ContactDetails21 details = this._objFactory.createContactDetails21();
		details.setMobNb(phoneNo);
		details.setEmailAdr(emailAddr);
		pi431.setCtctDtls(details);
		return pi431;
	}
	

}
