package com.formssi.business.local.cto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.cglib.beans.BeanMap;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.formssi.business.common.entity.SimTxBase;
import com.formssi.business.common.entity.SimVO_Pacs008;
import com.formssi.business.common.entity.SimVO_Pacs008_CdtTrfTxInf;
import com.formssi.msg.SimLocalFileConvertor;
import com.formssi.utils.SimStringUtils;

@Component("LOCAL.CTO")
@Scope("prototype")
public class SimGenF_CTO extends SimTxBase {

	@Override
	public void perform() throws Exception {
		SimVO_Pacs008 vo = (SimVO_Pacs008)txVo;
		SimMsg_CTO msg = new SimMsg_CTO(vo);
		SimLocalFileConvertor.getInstance().convertToSendXmlFile(msg, this.dirName);
	}

	@Override
	public boolean validate() throws Exception {
		return true;
	}

	@Override
	public void parseLocalFileData(Map<String, Object> dataMap, String dirName) throws Exception {
		if(dataMap.isEmpty()){
			return ;
		}
		
		if ("LOCAL.CTO".equals(this.serviceName)){
			
			txVo = new SimVO_Pacs008();
			SimVO_Pacs008 vo = (SimVO_Pacs008)txVo;
			
			this.dirName = dirName;
			
			List<SimVO_Pacs008_CdtTrfTxInf> cdtTrfTxInfList = new ArrayList<>();
			SimVO_Pacs008_CdtTrfTxInf cdtTrfTxInf = new SimVO_Pacs008_CdtTrfTxInf();
			cdtTrfTxInf = mapToBean(dataMap, cdtTrfTxInf);
			
			if(!SimStringUtils.isEmptyOrNull((String)dataMap.get("bizSvc")))
				txVo.setBizSvc((String)dataMap.get("bizSvc"));
			
			cdtTrfTxInfList.add(cdtTrfTxInf);
			vo.setCdtTrfTxInfList(cdtTrfTxInfList);
		}
	}
	
	/** 
	 * 将map装换为javabean对象 
	 * @param map 
	 * @param bean 
	 * @return 
	 */
	private static <T> T mapToBean(Map<String, Object> map,T bean) { 
	  BeanMap beanMap = BeanMap.create(bean); 
	  beanMap.putAll(map); 
	  return bean; 
	} 
	
}
