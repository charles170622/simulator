package com.formssi.business.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.formssi.bean.FileDataBean;
import com.formssi.business.common.entity.SimTxBase;
import com.formssi.defines.SimConstants;
import com.formssi.utils.SimStringUtils;

@Component("SimFileReadHelper")
public class SimFileReadHelper extends SimHelper{
	
	@Autowired
	private BeanFactory beanFactory;

	public boolean helpFileRead(FileDataBean fileDataBean) throws Exception{
		
		if(SimStringUtils.isEmptyOrNull(fileDataBean.getDirName())){
			return false;
		}
		
		LinkedList<Map<String, Object>> sumMapList = new LinkedList<>();
		
		List<Map<String,Object>> mapListCto = fileDataBean.getMapListCto();
		List<Map<String,Object>> mapListDdo = fileDataBean.getMapListDdo();
		List<Map<String,Object>> mapListRro = fileDataBean.getMapListRro();
		
		sumMapList.addAll(mapListCto);
		sumMapList.addAll(mapListDdo);
		sumMapList.addAll(mapListRro);
		
		//compare with local files
		File directory = new File(SimConstants.BANKSIM_PATH_SEND_REQ + fileDataBean.getDirName());
		if(directory.isDirectory()){
			File[] files = directory.listFiles();
			for(File f: files){
				boolean delFlag = false;
				String txId = f.getName().split("_")[1];
				
				Iterator<Map<String, Object>> iterator = sumMapList.iterator();
				while(iterator.hasNext()){
					Map<String, Object> data = iterator.next();
					String txIdNew = (String)data.get("txId");
					if(txId.equalsIgnoreCase(txIdNew)){
						delFlag = false;
						break;
					}else{
						delFlag = true;
					}
				}
				if(delFlag){
					f.delete();
				}
			}
		}
		
		if(mapListCto != null && mapListCto.size()>0){
			handleFileData(mapListCto, SimConstants.SIM_XML_TP_CTO, fileDataBean.getDirName());
		}
		
		if(mapListDdo != null && mapListDdo.size()>0){
			handleFileData(mapListDdo, SimConstants.SIM_XML_TP_DDO, fileDataBean.getDirName());
		}
		
		if(mapListRro != null && mapListRro.size()>0){
			handleFileData(mapListRro, SimConstants.SIM_XML_TP_RRO, fileDataBean.getDirName());
		}
		
		return true;
	}
	
	private void handleFileData(List<Map<String,Object>> mapList, String type, String dirName) throws Exception{
		for(Map<String, Object> dataMap : mapList){
			String serviceName = SimConstants.TX_SOURCE_LOCAL + "." + type;
			SimTxBase txBase = (SimTxBase) this.beanFactory.getBean(serviceName);

			if (txBase != null) {
				txBase.setServiceName(serviceName);
				txBase.parseLocalFileData(dataMap, dirName);
				if (txBase.validate()) {
					txBase.perform();
				}
			}
		}
	}
	
}
