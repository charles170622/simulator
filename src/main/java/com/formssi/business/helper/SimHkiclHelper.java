package com.formssi.business.helper;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.forms.ffp.adaptor.jaxb.iclfps.fps_envelope_01.ISO20022BusinessDataV01;
import com.forms.ffp.adaptor.jaxb.iclfps.head_001_001_01.BusinessApplicationHeaderV01;
import com.formssi.business.common.entity.SimTxBase;
import com.formssi.defines.SimConstants;
import com.formssi.msg.iclfps.FFPISO20022MessageWrapper;

@Component
@Scope("prototype")
public class SimHkiclHelper extends SimHelper
{
	@Autowired
	private BeanFactory beanFactory;
	
	public boolean helperHkicl(String inwardorOutward, ISO20022BusinessDataV01 bizData, FFPISO20022MessageWrapper wrapper) throws Exception
	{
		BusinessApplicationHeaderV01 head = (BusinessApplicationHeaderV01)bizData.getContent().get(0).getValue();

		String msgtype = head.getMsgDefIdr();
		String serviceName = SimConstants.TX_SOURCE_HKICL + "." + msgtype;
		SimTxBase txBase = (SimTxBase) this.beanFactory.getBean(serviceName);
		if (txBase != null)
		{
			txBase.setServiceName(serviceName);
			
			txBase.parseISO20022BizData(bizData);
			
			txBase.setFFPISO20022MessageWrapper(wrapper);
			boolean valid = txBase.validate();
			if(valid){
				txBase.perform();
			}
		}
		return true;
	}
}
