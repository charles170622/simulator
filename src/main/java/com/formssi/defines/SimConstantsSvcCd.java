package com.formssi.defines;

public class SimConstantsSvcCd{

	public final static String ICLFPS_SERVICECODE_PAYC01 = "PAYC01";
	
	public final static String ICLFPS_SERVICECODE_PAYC02 = "PAYC02";
	
	public final static String ICLFPS_SERVICECODE_PAYC03 = "PAYC03";
	
	public final static String ICLFPS_SERVICECODE_PAYD01 = "PAYD01";
	
	public final static String ICLFPS_SERVICECODE_PAYR01 = "PAYR01";
	
	public final static String ICLFPS_SERVICECODE_PAYNTF = "PAYNTF";
	
	public final static String ICLFPS_SERVICECODE_PAYENQ = "PAYENQ";
	
	public final static String ICLFPS_SERVICECODE_ADMISV = "ADMISV";
	
	public final static String ICLFPS_SERVICECODE_ADDR01 = "ADDR01";
	
}