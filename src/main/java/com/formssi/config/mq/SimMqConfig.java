package com.formssi.config.mq;

import java.util.Map;

import com.formssi.config.ISimConnectorConfig;

public class SimMqConfig implements ISimConnectorConfig{
	
	private String connectorName;
	
	private String connectorType;
	
	private SimQueueManagerConfig qmCfg;
	
	private Map<String, SimQueueConfig> receiveQueueNameMap;

	private SimQueueConfig outwardRequestHighPriorityQueueName;
	
	private SimQueueConfig outwardRequestMediumPriorityQueueName;
	
	private SimQueueConfig outwardAcknowledgeHighPriorityQueueName;
	
	private SimQueueConfig outwardAcknowledgeMediumPriorityQueueName;
	
	

	private String defaultDestinationQueueName;

	private String inwardSelector;
	
	private Integer sessionCacheSize;

	public String getConnectorName() {
		return connectorName;
	}

	public void setConnectorName(String connectorName) {
		this.connectorName = connectorName;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public SimQueueManagerConfig getQmCfg() {
		return qmCfg;
	}

	public void setQmCfg(SimQueueManagerConfig qmCfg) {
		this.qmCfg = qmCfg;
	}

	public Map<String, SimQueueConfig> getReceiveQueueNameMap() {
		return receiveQueueNameMap;
	}

	public void setReceiveQueueNameMap(Map<String, SimQueueConfig> receiveQueueNameMap) {
		this.receiveQueueNameMap = receiveQueueNameMap;
	}

	public SimQueueConfig getOutwardRequestHighPriorityQueueName() {
		return outwardRequestHighPriorityQueueName;
	}

	public void setOutwardRequestHighPriorityQueueName(SimQueueConfig outwardRequestHighPriorityQueueName) {
		this.outwardRequestHighPriorityQueueName = outwardRequestHighPriorityQueueName;
	}

	public SimQueueConfig getOutwardRequestMediumPriorityQueueName() {
		return outwardRequestMediumPriorityQueueName;
	}

	public void setOutwardRequestMediumPriorityQueueName(SimQueueConfig outwardRequestMediumPriorityQueueName) {
		this.outwardRequestMediumPriorityQueueName = outwardRequestMediumPriorityQueueName;
	}

	public SimQueueConfig getOutwardAcknowledgeHighPriorityQueueName() {
		return outwardAcknowledgeHighPriorityQueueName;
	}

	public void setOutwardAcknowledgeHighPriorityQueueName(SimQueueConfig outwardAcknowledgeHighPriorityQueueName) {
		this.outwardAcknowledgeHighPriorityQueueName = outwardAcknowledgeHighPriorityQueueName;
	}

	public SimQueueConfig getOutwardAcknowledgeMediumPriorityQueueName() {
		return outwardAcknowledgeMediumPriorityQueueName;
	}

	public void setOutwardAcknowledgeMediumPriorityQueueName(SimQueueConfig outwardAcknowledgeMediumPriorityQueueName) {
		this.outwardAcknowledgeMediumPriorityQueueName = outwardAcknowledgeMediumPriorityQueueName;
	}

	public String getDefaultDestinationQueueName() {
		return defaultDestinationQueueName;
	}

	public void setDefaultDestinationQueueName(String defaultDestinationQueueName) {
		this.defaultDestinationQueueName = defaultDestinationQueueName;
	}

	public String getInwardSelector() {
		return inwardSelector;
	}

	public void setInwardSelector(String inwardSelector) {
		this.inwardSelector = inwardSelector;
	}

	public Integer getSessionCacheSize() {
		return sessionCacheSize;
	}

	public void setSessionCacheSize(Integer sessionCacheSize) {
		this.sessionCacheSize = sessionCacheSize;
	}

	

}
