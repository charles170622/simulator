package com.formssi.config;

public interface ISimConnectorConfig {
	
	String getConnectorName();
	
	String getConnectorType();

}
