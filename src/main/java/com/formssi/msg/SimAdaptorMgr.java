package com.formssi.msg;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formssi.connector.sender.mq.SimMqSenderAgent;
import com.formssi.defines.SimConstants;
import com.formssi.msg.iclfps.SimMsgBaseHkiclMessage;
import com.formssi.msg.iclfps.SimSendMessageResp;
import com.formssi.service.SimSenderAgentSvc;
import com.formssi.utils.FFPDateUtils;

public class SimAdaptorMgr
{
	public static final Class<SimAdaptorMgr> CLASS_NAME = SimAdaptorMgr.class;
	
//	public static final String ERROR_CODE = FFPTeErrorMsg.getErrorCode(CLASS_NAME, 0);
	
	private static Logger _logger = LoggerFactory.getLogger(SimAdaptorMgr.class);
	
	private static SimAdaptorMgr instance = null;

	private SimAdaptorMgr(){
		
	}

	public static SimAdaptorMgr getInstance(){
		if (instance == null)
			instance = new SimAdaptorMgr();
		return instance;
	}
	

	public SimSendMessageResp execute(SimMsgBaseHkiclMessage ip_object){
		
		SimSendMessageResp resp = null;
		SimMsgBaseHkiclMessage messageObj = ip_object;
		String message = "";
		
		try{
			
			message = messageObj.parseHkiclMessage();
			
			System.out.println("--------------------message   start----------------------");
			System.out.println(message);
			System.out.println("--------------------message   end----------------------");
			
			SimMqSenderAgent sender = (SimMqSenderAgent) SimSenderAgentSvc.getSenderAgent(SimConstants.CONNECTOR_NAME);
			if (SimConstants.SEND_TYPE_REQ.equals(messageObj.getSendType()))
			{
				resp = sender.sendRequest(message, null, messageObj.getPriority());
			} 
			else if (SimConstants.SEND_TYPE_ACK.equals(messageObj.getSendType()))
			{
				resp = sender.sendAcknowledge(message, null, messageObj.getPriority());
			}
//			resp.setMessageStatus(FFPConstantsTxJnl.MSG_STATUS.MSG_STAT_MSYNC.getStatus());
			resp.setMessage(String.format("The message is sent to target MQ. (%s)", new Object[] { FFPDateUtils.getDateStr(new Date(resp.getSentSysTime().longValue()), "yyyy-MM-dd HH:mm:ss") }));
			
		} 
		catch (Exception e){
			_logger.error("FFPBaseResp.execute(FFPMsgBaseHkiclMessage)Error Msg:" + e.getMessage());
			if(resp == null){
				resp = new SimSendMessageResp();
			}
//			resp.setMessageStatus(SimConstantsTxJnl.MSG_STATUS.MSG_STAT_REJCT.getStatus());
			resp.setMessage(String.format("The message isn't sent to target MQ. (%s)", new Object[] { FFPDateUtils.getDateStr(new Date(), "yyyy-MM-dd HH:mm:ss") }));
			resp.setSentSysTime(System.currentTimeMillis());
		}
		return resp;
		
	}


}
