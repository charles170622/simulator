package com.formssi.msg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formssi.bean.SimCTOReceiveBean;
import com.formssi.business.common.entity.SimVOBase;
import com.formssi.business.common.entity.SimVO_Pacs002;
import com.formssi.defines.SimConstants;
import com.formssi.msg.iclfps.SimMsgBaseHkiclMessage;
import com.formssi.service.SimBridgeConfigSvc;
import com.formssi.utils.SimFileNameUtils;
import com.formssi.utils.SimStringUtils;

public class SimLocalFileConvertor {
	// public static final Class<SimLocalFileConvertor> CLASS_NAME =
	// SimLocalFileConvertor.class;

	// public static final String ERROR_CODE =
	// FFPTeErrorMsg.getErrorCode(CLASS_NAME, 0);

	private static Logger _logger = LoggerFactory.getLogger(SimLocalFileConvertor.class);

	private static SimLocalFileConvertor instance = null;

	private SimLocalFileConvertor() {
	}

	public static SimLocalFileConvertor getInstance() {
		if (instance == null) {
			synchronized (SimLocalFileConvertor.class) {
				if (instance == null) {
					instance = new SimLocalFileConvertor();
				}
			}
		}
		return instance;
	}

	public void commonWrite2Local(SimBridgeConfigSvc bridgeConfig) {
		LinkedBlockingQueue<SimCTOReceiveBean> queue = bridgeConfig.getListenerCommonQueue();
		if (bridgeConfig == null || queue.size() == 0)
			return;

		int rowNum = 1;
		while (true) {
			SimCTOReceiveBean receiveBean = queue.poll();
			if (receiveBean == null) {
				break;
			}
			// write to file
			XSSFWorkbook xWorkbook = null;
			FileInputStream fileIn = null;
			FileOutputStream fileOut = null;

			try {
				String path = bridgeConfig.getReceiveAckMsgPath();
				String dirName = path.substring(path.lastIndexOf("/") + 1);
				String filename = SimConstants.BANKSIM_PATH_RECEIVE_ACK + dirName + "/" + dirName + "_ACK.xlsx";

				fileIn = new FileInputStream(filename);
				xWorkbook = new XSSFWorkbook(fileIn);
				XSSFSheet sheet1 = xWorkbook.getSheetAt(0);
				if (sheet1 == null) {
					return;
				}
				fileOut = new FileOutputStream(filename);

				Field[] fields = SimCTOReceiveBean.class.getDeclaredFields();
				Row row = sheet1.createRow(rowNum);
				rowNum++;
				for (int i = 0; i < fields.length - 1; i++) {
					Field field = fields[i];
					field.setAccessible(true);

					String value = (String) field.get(receiveBean);
					Cell cell = row.createCell(i);
					cell.setCellType(CellType.STRING);
					cell.setCellValue(value);
				}
				xWorkbook.write(fileOut);
			} catch (SecurityException | IllegalArgumentException | IllegalAccessException | IOException e) {
				_logger.error("SimLocalFileConvertor.commonWrite2Local()Error Msg:" + e.getMessage());
			} finally {
				try {
					fileOut.close();
					xWorkbook.close();
				} catch (IOException e) {
					_logger.error("XWorkbook or FileOut Close Error", e);
				}
			}
			
			FileWriter fw = null;
			try {
				String path = bridgeConfig.getReceiveAckMsgPath();
				Map<String, String> infoMap = receiveBean.getFileInfoMap();
				if (infoMap != null && infoMap.size() > 0) {
					for(String fileName: infoMap.keySet()){
						String msgContent = infoMap.get(fileName);
						File file = new File(path, fileName);
						fw = new FileWriter(file);

						fw.write(msgContent);
						fw.flush();
						fw.close();
					}
				}
			} catch (IOException e) {
				_logger.error("Generate Receive XML File Error", e);
			} 
		}

	}

	// private synchronized boolean write2StatisticExcel(SimVOBase vo,
	// SimBridgeConfigSvc bridgeConfig) {
	// SimCTOReceiveBean bean = new SimCTOReceiveBean();
	//
	// if (vo instanceof SimVO_Pacs002) {
	// SimVO_Pacs002 locVo = (SimVO_Pacs002) vo;
	// SimVO_Pacs002_TxInfAndSts txInfo = locVo.getTxInfList().get(0);
	//
	// bean.setTransactionID(txInfo.getOrgnlTxId());
	// bean.setTransactionType(locVo.getBizSvc());
	// bean.setMessageID(locVo.getMsgId());
	// bean.setMessageType(locVo.getMsgDefIdr());
	// bean.setStatus(txInfo.getTxSts());
	// bean.setCurrency(txInfo.getSettlementCurrency());
	// String amount = null;
	// if (txInfo.getSettlementAmount() != null) {
	// amount = String.valueOf(txInfo.getSettlementAmount());
	// }
	// bean.setAmount(amount);
	// bean.setRejectCode(txInfo.getTxStsRsnCode());
	//
	// // write to file
	// XSSFWorkbook xWorkbook = null;
	// FileInputStream fileIn = null;
	// FileOutputStream fileOut = null;
	// try {
	// String path = bridgeConfig.getReceiveAckMsgPath();
	// String dirName = path.substring(path.lastIndexOf("/") + 1);
	// String filename = SimConstants.BANKSIM_PATH_RECEIVE_ACK + dirName + "/" +
	// dirName + "_ACK.xlsx";
	//
	// fileIn = new FileInputStream(filename);
	// xWorkbook = new XSSFWorkbook(fileIn);
	// XSSFSheet sheet1 = xWorkbook.getSheetAt(0);
	// if (sheet1 == null) {
	// return false;
	// }
	// int lastRowNum = sheet1.getLastRowNum();
	//
	// fileOut = new FileOutputStream(filename);
	// // write field
	// Field[] fields = SimCTOReceiveBean.class.getDeclaredFields();
	// Row row = sheet1.createRow(lastRowNum + 1);
	// for (int i = 0; i < fields.length; i++) {
	// Field field = fields[i];
	// field.setAccessible(true);
	// String value = (String) field.get(bean);
	//
	// Cell cell = row.createCell(i);
	// cell.setCellType(CellType.STRING);
	// ;
	// cell.setCellValue(value);
	// }
	// xWorkbook.write(fileOut);
	//
	// } catch (Exception e) {
	// _logger.error("Method write2StatisticExcel(SimVOBase vo, String
	// dirName)Error Msg:" + e.getMessage());
	// } finally {
	// try {
	// fileOut.close();
	// xWorkbook.close();
	// } catch (IOException e) {
	// _logger.error("XWorkbook or FileOut Close Error", e);
	// }
	// }
	// } else {
	//
	// }
	//
	// return true;
	// }

//	public boolean genereateReceiveXmlFile(SimVOBase vo, SimBridgeConfigSvc bridgeConfig) {
//		String path = bridgeConfig.getReceiveAckMsgPath();
//
//		if (SimStringUtils.isEmptyOrNull(path) || vo == null) {
//			return false;
//		}
//
//		if (vo instanceof SimVO_Pacs002) {
//			SimVO_Pacs002 loc_pacs002 = (SimVO_Pacs002) vo;
//
//			String txId = loc_pacs002.getTxInfList().get(0).getOrgnlTxId();
//			String fileName = SimFileNameUtils.getFileName(vo.getBizSvc(), txId);
//			String msgContent = null;
//			try {
//				msgContent = vo.getIclfpsWrapper().getMessage().getText();
//				if (!SimStringUtils.isEmptyOrNull(fileName)) {
//					File file = new File(path, fileName);
//					FileWriter fw = new FileWriter(file);
//
//					fw.write(msgContent);
//					fw.flush();
//					fw.close();
//				}
//
//			} catch (Exception e) {
//				_logger.error("SimLocalFileConvertor.convertToReceiveXmlFile(..) error", e);
//			}
//		}
//
//		return true;
//	}

	public boolean convertToSendXmlFile(SimMsgBaseHkiclMessage ip_object, String dirName) {
		SimMsgBaseHkiclMessage messageObj = ip_object;
		String message = "";
		String filename = "";
		try {
			message = messageObj.parseHkiclMessage();
			if (!SimStringUtils.isEmptyOrNull(message)) {
				// file name
				// String xmlType = messageObj.getXmlType();
				String txId = messageObj.getTxId();

				filename = SimFileNameUtils.getFileName(messageObj.getMsgBizSvc(), txId);
				if (!SimStringUtils.isEmptyOrNull(filename)) {
					String fileSavePath = SimConstants.BANKSIM_PATH_SEND_REQ + dirName;
					File dir = new File(fileSavePath);
					if (!dir.exists()) {
						dir.mkdir();
					}

					File newFile = new File(fileSavePath, filename);
					// if file had exists ,by txId ,delete
					if (dir.isDirectory()) {
						File[] files = dir.listFiles();
						for (File f : files) {
							String existFileName = f.getName().split("_")[1];
							String newFileName = newFile.getName().split("_")[1];
							if (existFileName.equalsIgnoreCase(newFileName)) {
								f.delete();
							}
						}
					}
					// not exist , create
					if (newFile.createNewFile()) {
						// write to file
						OutputStream os = new FileOutputStream(newFile);
						os.write(message.getBytes());
						os.flush();
						os.close();
					}
				}
			}
		} catch (Exception e) {
 			_logger.error("FFPBaseResp.execute(FFPMsgBaseHkiclMessage)Error Msg:" + e.getMessage());
		}
		return true;
	}
}
