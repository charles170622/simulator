package com.formssi.form;

import java.io.Serializable;

public class SimReturnBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String returnMsg;
	
	private String returnCode;

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
	

}
