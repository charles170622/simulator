package com.formssi.form;

import java.io.Serializable;

public class FileInfoForm implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String txId;
	
	private String fileName;
	
	private String crtTm;
	
	
	
	public String getTxId() {
		return txId;
	}

	public void setTxId(String txId) {
		this.txId = txId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCrtTm() {
		return crtTm;
	}

	public void setCrtTm(String crtTm) {
		this.crtTm = crtTm;
	}
	
	

}
