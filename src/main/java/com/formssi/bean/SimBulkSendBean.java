package com.formssi.bean;

public class SimBulkSendBean {
	
	private String msg;
	
	private String priority;

	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	
	
	

}
