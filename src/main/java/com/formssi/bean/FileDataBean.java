package com.formssi.bean;

import java.util.List;
import java.util.Map;

public class FileDataBean {
	
	List<Map<String, Object>> mapListCto;
	
	List<Map<String, Object>> mapListDdo;
	
	List<Map<String, Object>> mapListRro;
	
	private String dirName;
	
	
	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public List<Map<String, Object>> getMapListCto() {
		return mapListCto;
	}

	public void setMapListCto(List<Map<String, Object>> mapListCto) {
		this.mapListCto = mapListCto;
	}

	public List<Map<String, Object>> getMapListDdo() {
		return mapListDdo;
	}

	public void setMapListDdo(List<Map<String, Object>> mapListDdo) {
		this.mapListDdo = mapListDdo;
	}

	public List<Map<String, Object>> getMapListRro() {
		return mapListRro;
	}

	public void setMapListRro(List<Map<String, Object>> mapListRro) {
		this.mapListRro = mapListRro;
	}
	
	

}
