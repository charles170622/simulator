package com.formssi.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimCTOReceiveBean {
	
	private String transactionID;
	
	private String messageID;
	
	private String currency;
	
	private String amount;
	
	private String status;
	
	private String transactionType;
	
	private String messageType;
	
	private String rejectCode;
	
	private Map<String, String> fileInfoMap;
	
	
	public Map<String, String> getFileInfoMap() {
		if(fileInfoMap == null){
			fileInfoMap = new HashMap<String,String>();
		}
		return fileInfoMap;
	}


	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getRejectCode() {
		return rejectCode;
	}

	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}
	
	
	
	
}
