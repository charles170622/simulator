package com.formssi.connector.sender.mq;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.formssi.config.mq.SimMqConfig;
import com.formssi.connector.sender.ISimMqSenderAgentInterface;
import com.formssi.defines.SimConstants;
import com.formssi.msg.iclfps.SimSendMessageResp;
import com.formssi.msg.iclfps.creator.SimMsgCreator;
import com.formssi.msg.iclfps.creator.SimMsgProperty;


public class SimMqSenderAgent implements ISimMqSenderAgentInterface{
	
	private static Logger logger = LoggerFactory.getLogger(SimMqSenderAgent.class);

	protected SimMqConfig config;

	protected JmsTemplate jmsTemplate;

	public SimMqSenderAgent(SimMqConfig config){
		this.config = config;
		init();
	}

	public void init(){
	}

	public SimSendMessageResp sendAcknowledge(String message, Map<String, Object> propertyMap, String priority){
		
		String destinationName = this.jmsTemplate.getDefaultDestinationName();
		
		if(SimConstants.MQ_LEVEL_PRIORITY_HIGH.equals(priority)){
			destinationName = "queue:///" + this.config.getOutwardAcknowledgeHighPriorityQueueName().getQueueName();
		}else if(SimConstants.MQ_LEVEL_PRIORITY_MEDIUM.equals(priority)){
			destinationName = "queue:///" + this.config.getOutwardAcknowledgeMediumPriorityQueueName().getQueueName();
		}
		
		return send(message, propertyMap, destinationName);
	}

	public SimSendMessageResp sendRequest(String message, Map<String, Object> propertyMap, String priority){
		
		String destinationName = this.jmsTemplate.getDefaultDestinationName();
		if(SimConstants.MQ_LEVEL_PRIORITY_HIGH.equals(priority)){
			destinationName = "queue:///" + this.config.getOutwardRequestHighPriorityQueueName().getQueueName();
		}else if(SimConstants.MQ_LEVEL_PRIORITY_MEDIUM.equals(priority)){
			destinationName = "queue:///" + this.config.getOutwardRequestMediumPriorityQueueName().getQueueName();
		}
		return send(message, propertyMap, destinationName);
	}
	
	
	public SimSendMessageResp send(String message, Map<String, Object> propertyMap, String destinationName) {
		SimSendMessageResp resp = new SimSendMessageResp();
		resp.setDestination(destinationName);

		if (propertyMap == null){
			propertyMap = new HashMap<String, Object>();
		}

		SimMsgCreator msgCreator = new SimMsgCreator();
		msgCreator.setMsgObj(message);

		SimMsgProperty msgProperty = new SimMsgProperty();
		msgProperty.setPropertyMap(propertyMap);

		msgCreator.setMsgProperty(msgProperty);
		msgCreator.setMsgConverter(this.jmsTemplate.getMessageConverter());

		if (logger.isDebugEnabled()){
			logger.debug(String.format("Send Message to MQ [%s]. Content Length: %s, Property: %s", new Object[] { destinationName, Integer.valueOf(message.length()), propertyMap }));
		}

		Long startTime = Long.valueOf(System.currentTimeMillis());
		this.jmsTemplate.send(destinationName, msgCreator);

		Long diff = Long.valueOf(System.currentTimeMillis() - startTime.longValue());

		if (logger.isInfoEnabled()){
			logger.info(String.format("Send Message([%s]) to MQ [%s] Completed. Total time [%s]ms", new Object[] {message, destinationName, diff }));
		}
		
		try{
			resp.setJmsMessageId(msgCreator.getMsg().getJMSMessageID());
			resp.setSentSysTime(Long.valueOf(msgCreator.getMsg().getJMSTimestamp()));
		} catch (Exception e){
			if (logger.isErrorEnabled()){
				logger.error("Fail to retrieve JMS Message Information", e);
			}
		}

		return resp;
	}

	public SimMqConfig getConfig()
	{
		return this.config;
	}

	public void setConfig(SimMqConfig config){
		this.config = config;
	}

	public JmsTemplate getJmsTemplate()
	{
		return this.jmsTemplate;
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate)
	{
		this.jmsTemplate = jmsTemplate;
	}
}
