package com.formssi.connector.sender.mq;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import javax.net.ssl.SSLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;

import com.formssi.config.mq.SimMqConfig;
import com.formssi.config.mq.SimQueueManagerConfig;
import com.formssi.defines.SimConstants;
import com.formssi.msg.iclfps.SimSendMessageResp;
import com.formssi.utils.FFPSecurityUtils;
import com.ibm.mq.jms.MQXAQueueConnectionFactory;

public class SimIbmWebSphereMqSenderAgent extends SimMqSenderAgent {
	private static Logger logger = LoggerFactory.getLogger(SimIbmWebSphereMqSenderAgent.class);

	public SimIbmWebSphereMqSenderAgent(SimMqConfig config) {
		super(config);
	}

	public void init() {

		if (logger.isInfoEnabled()) {
			logger.info("MQ Sender Agent Initialization start.......");
		}
		try {

			this.jmsTemplate = new JmsTemplate();

			SimQueueManagerConfig qmCfg = this.config.getQmCfg();

			MQXAQueueConnectionFactory factory = new MQXAQueueConnectionFactory();
			factory.setHostName(qmCfg.getHostName());
			factory.setPort(qmCfg.getPort().intValue());
			factory.setQueueManager(qmCfg.getQueueManagerName());
			factory.setChannel(qmCfg.getClientChannel());
			factory.setCCSID(qmCfg.getCcsid().intValue());
			factory.setTransportType(1);

			if ((qmCfg.getSslEnable() != null) && (qmCfg.getSslEnable().booleanValue())) {

				factory.setSSLPeerName(qmCfg.getSslPeerName());
				factory.setSSLCipherSuite(qmCfg.getSslCipherSuite());

				try {
					SSLContext sslContext = FFPSecurityUtils.createSSLContext(qmCfg.getSslTrustStoreFilename(),
							qmCfg.getSslTrustStorePassword(), "JKS", qmCfg.getSslKeyStoreFilename(),
							qmCfg.getSslKeyStorePassword(), "JKS");

					factory.setSSLSocketFactory(sslContext.getSocketFactory());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				// try{
				// SSLContext sslContext =
				// FFPSecurityUtils.createSSLContext(FFPRuntimeConfigSvc.getInstance().getConfigFilePath(qmCfg.getSslTrustStoreFilename()),
				// qmCfg.getSslTrustStorePassword(),
				// "JKS",
				// FFPRuntimeConfigSvc.getInstance().getConfigFilePath(qmCfg.getSslKeyStoreFilename()),
				// qmCfg.getSslKeyStorePassword(), "JKS");
				//
				// factory.setSSLSocketFactory(sslContext.getSocketFactory());
				// } catch (Exception e){
				// throw new RuntimeException(e);
				// }
			}

			UserCredentialsConnectionFactoryAdapter adapter = new UserCredentialsConnectionFactoryAdapter();
			adapter.setTargetConnectionFactory(factory);
			adapter.setUsername(qmCfg.getUser());
			adapter.setPassword(qmCfg.getPassword());

			CachingConnectionFactory ccf = new CachingConnectionFactory();
			ccf.setTargetConnectionFactory(adapter);
			ccf.setSessionCacheSize(this.config.getSessionCacheSize().intValue());

			if (logger.isInfoEnabled()) {
				logger.info(
						"Connection Factory initialized [{}] Host: [{}:{}] QM: [{}], Client Channel: [{}], CCSID: [{}], SSL Enabled: [{}]",
						new Object[] { ccf.toString(), qmCfg.getHostName(), qmCfg.getPort(),
								qmCfg.getQueueManagerName(), qmCfg.getClientChannel(), qmCfg.getCcsid(),
								qmCfg.getSslEnable() });
			}

			this.jmsTemplate.setConnectionFactory(ccf);
			this.jmsTemplate.setSessionTransacted(true);
			this.jmsTemplate.setPubSubDomain(false);
			this.jmsTemplate.setDefaultDestinationName("queue:///" + config.getDefaultDestinationQueueName());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public SimSendMessageResp sendAcknowledge(String message, Map<String, Object> propertyMap, String priority) {

		String destinationName = this.jmsTemplate.getDefaultDestinationName();
		if (SimConstants.MQ_LEVEL_PRIORITY_HIGH.equals(priority)) {
			destinationName = "queue:///" + config.getOutwardAcknowledgeHighPriorityQueueName().getQueueName();
		} else if (SimConstants.MQ_LEVEL_PRIORITY_MEDIUM.equals(priority)) {
			destinationName = "queue:///" + config.getOutwardAcknowledgeMediumPriorityQueueName().getQueueName();
		}

		return send(message, propertyMap, destinationName);
	}

	public SimSendMessageResp sendRequest(String message, Map<String, Object> propertyMap, String priority) {

		String destinationName = this.jmsTemplate.getDefaultDestinationName();
		if (SimConstants.MQ_LEVEL_PRIORITY_HIGH.equals(priority)) {
			destinationName = "queue:///" + config.getOutwardRequestHighPriorityQueueName().getQueueName();
		} else if (SimConstants.MQ_LEVEL_PRIORITY_MEDIUM.equals(priority)) {
			destinationName = "queue:///" + config.getOutwardRequestMediumPriorityQueueName().getQueueName();
		}
		return send(message, propertyMap, destinationName);
	}

	@Async("bulkSendExecutor")
	public void bulkSendRequest(String message, String priority) {
		logger.info("==================START EXECUTEASYNC==================");

		try {
			String destinationName = this.jmsTemplate.getDefaultDestinationName();
			if (SimConstants.MQ_LEVEL_PRIORITY_HIGH.equals(priority)) {
				destinationName = "queue:///" + config.getOutwardRequestHighPriorityQueueName().getQueueName();
			} else if (SimConstants.MQ_LEVEL_PRIORITY_MEDIUM.equals(priority)) {
				destinationName = "queue:///" + config.getOutwardRequestMediumPriorityQueueName().getQueueName();
			}
			send(message, null, destinationName);
		} catch (Exception e) {
			logger.error("Send To IBM MQ Occur Errors", e);
		}
		
		logger.info("==================END EXECUTEASYNC==================");
	}

}
