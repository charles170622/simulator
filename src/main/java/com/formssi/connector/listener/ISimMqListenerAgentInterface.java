package com.formssi.connector.listener;

import org.springframework.stereotype.Component;

@Component("ISimMqListenerAgentInterface")
public interface ISimMqListenerAgentInterface {

	boolean getStartWithException();

	void init() throws Exception;

	void startListeners() throws Exception;

	void onDestory() throws Exception;
}
