package com.formssi.connector.listener.msg;

import java.sql.Timestamp;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formssi.msg.FFPMessageWrapper;
import com.formssi.utils.FFPDateUtils;

public abstract class SimBaseMsgListener
{
	private static Logger _logger = LoggerFactory.getLogger(SimBaseMsgListener.class);
	
	public abstract boolean onValidateSignature(TextMessage message, boolean ignoreException) throws Exception;

	public abstract FFPMessageWrapper onMessageParse(TextMessage message, String queueName, String priority) throws Exception;
	
	public abstract void handleMessage(FFPMessageWrapper warpper);
	
	public void onMessage(Message message, String queueName, String priority)
	{
		Long sysTime = Long.valueOf(System.currentTimeMillis());
		FFPMessageWrapper warpper = null;
		String jmsId = null;
		try
		{
			jmsId = message.getJMSMessageID();
			if (!(message instanceof TextMessage))
			{
				_logger.error(String.format("[%s]>> Skip handling message due to message is not TextMessage: %s", new Object[] { queueName, message.getJMSMessageID() }));
				return;
			}
			
			TextMessage textMessage = (TextMessage) message;
			
			_logger.info(String.format("[%s]Receive JmsId=[%s] Msg=[%s]", new Object[] {queueName, message.getJMSMessageID(), textMessage.getText() }));
			if(!onValidateSignature(textMessage, true))
			{
				return;
			};
			
			warpper = onMessageParse(textMessage, queueName, priority);

			if (_logger.isInfoEnabled())
			{
				_logger.info(String.format("[queueName %s]>> Message received on [%s].",
						new Object[] { queueName, sysTime != null ? FFPDateUtils.getTimeStr(new Timestamp(sysTime.longValue()), "yyyy-MM-dd-HH.mm.ss.SSS") : "null" }));
			}
			
		}
		catch (Exception e){
			_logger.error(String.format("[%s]>> Failed to parse message:[%s]", new Object[] { queueName, jmsId}), e);
			return;
		}
		
		handleMessage(warpper);
		
	}
	
	
}
