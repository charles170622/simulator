package com.formssi.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.formssi.defines.SimConstants;
import com.formssi.form.FileInfoForm;
import com.formssi.form.SimReturnBean;
import com.formssi.service.SimBridgeConfigSvc;
import com.formssi.service.SimBulkHandlerSvc;
import com.formssi.utils.SimStringUtils;

@RestController
public class BulkController {

	private static Logger _logger = LoggerFactory.getLogger(BulkController.class);
	
	@Autowired
	private SimBulkHandlerSvc service;
	
	@Autowired
	private SimBridgeConfigSvc bridgeConfig;
	
	@RequestMapping(value = "/xmllist", method = RequestMethod.POST)
	public ModelAndView xmlList(@RequestParam(value="dirName", required=true)String dirName) {
		ModelAndView mv = new ModelAndView("bulksend");
		mv.addObject("dirName", dirName);
		return mv;
	}

	@RequestMapping(value = "/fileRead", method = RequestMethod.POST)
	public List<FileInfoForm> fileRead(@RequestParam(value="dirName", required=true)String dirName) {
		
		List<FileInfoForm> fileInfoList = new ArrayList<>();
		String path = SimConstants.BANKSIM_PATH + dirName;
		if (!SimStringUtils.isEmptyOrNull(dirName)) {
			fileInfoList = service.parseToXml(path);
		}
		return fileInfoList;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public SimReturnBean fileShow(String directory, String fileName) {
		SimReturnBean rb = new SimReturnBean();
		String msg = "";
		StringBuffer sb = new StringBuffer();
		FileReader fr = null;
		BufferedReader bReader = null;
		try {
			String filePath = SimConstants.BANKSIM_PATH_SEND_REQ + directory;
			fr = new FileReader(new File(filePath, fileName));
			bReader = new BufferedReader(fr);
	        String s = "";
	        while ((s =bReader.readLine()) != null) {
	            sb.append(s + "\n");
	        }
			msg = sb.toString();
			rb.setReturnCode("success");
			rb.setReturnMsg(msg);
		} catch (Exception e) {
			_logger.error("file read failed");
			rb.setReturnCode("error");
		} finally {
			try {
				bReader.close();
				fr.close();
			} catch (IOException e) {
				_logger.error("file stream close failed");
				rb.setReturnCode("error");
			}
		}
		return rb;
	}
	
	@RequestMapping("/async")
	public SimReturnBean sendXml(String directory) {
		SimReturnBean rb = new SimReturnBean();
		String path = "";
		
		if (SimStringUtils.isEmptyOrNull(directory)) {
			rb.setReturnCode("failed");
			rb.setReturnMsg("error directory");
			return rb;
		}
		path = SimConstants.BANKSIM_PATH_SEND_REQ + directory;
		
		Boolean mutiSend = service.bulkSend(path);
		if(mutiSend){
			rb.setReturnCode("success");
			rb.setReturnMsg("send success");
		}else{
			rb.setReturnCode("failed");
			rb.setReturnMsg("send error");
		}
		
		return rb;
	}
}
