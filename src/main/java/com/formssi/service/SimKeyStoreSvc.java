package com.formssi.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;

import com.formssi.config.keystore.FFPKeystoreConfig;

@Service
public class SimKeyStoreSvc
{
	private static SimKeyStoreSvc instance = null;
	
	public static String PARTICIPANT_KEYSTORE_NAME = "simulator.keystore";

	private Map<String, FFPKeystoreConfig> keystoreConfigMap = new HashMap<String, FFPKeystoreConfig>();

	public static SimKeyStoreSvc getInstance(){
		
		if (instance == null){
			instance = new SimKeyStoreSvc();
		}
		return instance;
	}

	public FFPKeystoreConfig getKeysotreConfig(String key) throws Exception
	{
		if (keystoreConfigMap.get(key) == null)
		{
//			String messageKeystoreFolder = FFPRuntimeConfigSvc.getInstance().getRootConfig().get("config.keystore.message.folder");
//			String keystoreCofigFile = FFPRuntimeConfigSvc.getInstance().getRootConfig().get("config.keystore.filename");
//			PropertiesFile proFile = FFPRuntimeConfigSvc.getInstance().getPropertiesFile(keystoreCofigFile);
			String messageKeystoreFolder = "keystore/";
			String keystoreConfigFile = "keystore.properties";
			Properties proFile = PropertiesLoaderUtils.loadAllProperties(keystoreConfigFile);
			
			FFPKeystoreConfig config = new FFPKeystoreConfig();

			String filename = (String) proFile.get(key + ".file");
			config.setFilename((String) proFile.get(key + ".file"));

//			Path filepath = FFPRuntimeConfigSvc.getInstance().getConfigFilePath(messageKeystoreFolder + filename);
			Path filepath = Paths.get(messageKeystoreFolder, new String[] { filename });
			config.setFilepath(filepath);
			
			config.setAlias((String) proFile.get(key + ".alias"));
			config.setPassword((String) proFile.get(key + ".password"));
			config.setType((String) proFile.get(key + ".type"));

			keystoreConfigMap.put(key, config);
		}
		return keystoreConfigMap.get(key);
	}

}