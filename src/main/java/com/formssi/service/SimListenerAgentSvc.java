package com.formssi.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.formssi.config.ISimConnectorConfig;
import com.formssi.config.mq.SimMqConfig;
import com.formssi.connector.listener.ISimMqListenerAgentInterface;
import com.formssi.connector.listener.mq.SimIbmWebSphereMqListenerAgent;
import com.formssi.defines.SimConstants;

@Component("SimListenerAgentSvc")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SimListenerAgentSvc extends Thread {
	private Logger _logger = LoggerFactory.getLogger(SimListenerAgentSvc.class);

	private Map<String, ISimMqListenerAgentInterface> listenerAgentMap = new HashMap<String, ISimMqListenerAgentInterface>();

	@Autowired
	private BeanFactory beanFactory;

	@PostConstruct
	public void init() throws Exception {
		initSimulatorListeners();
		startListener();
	}

	// initial listeners
	private void initSimulatorListeners() throws Exception {
		Map<String, ISimConnectorConfig> connectorConfigMap = SimConnectorConfigSvc.getInstance().getConnectorConfigMap();

		Iterator<String> iter = connectorConfigMap.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			ISimConnectorConfig connectorConfig = connectorConfigMap.get(key);

			if (_logger.isInfoEnabled()) {
				_logger.info("Start initialize Connector Config for: {}", connectorConfig.getConnectorName());
			}

			if (connectorConfig instanceof SimMqConfig) {
				SimMqConfig mqConfig = (SimMqConfig) connectorConfig;
				ISimMqListenerAgentInterface agent = null;

				if (SimConstants.CONNECTTOR_TYPE_MQ_WEBPHEREMQ.equals(mqConfig.getConnectorType())) {
					agent = (ISimMqListenerAgentInterface) this.beanFactory.getBean(SimIbmWebSphereMqListenerAgent.class,
							new Object[] { mqConfig });
				} else {
					throw new Exception(mqConfig.getConnectorName() + " type not define!");
				}

				agent.init();

				this.listenerAgentMap.put(key, agent);

			} else {
				throw new Exception(connectorConfig.getConnectorName() + " type not define!");
			}
		}

	}

	public boolean startListener() {
		_logger.info("Simulator Listeners will auto start");
		
		Iterator<String> iter = listenerAgentMap.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			ISimMqListenerAgentInterface agent = listenerAgentMap.get(key);
			_logger.info("FFP Listener will be start(key=" + key + ")");
			try {
				agent.startListeners();
			} catch (Exception e) {
				_logger.error("Start Listener failure " + agent.getClass().getName(), e);
			}
			if (agent.getStartWithException()) {
				_logger.error("Start Listener failure " + agent.getClass().getName());
				break;
			}
			_logger.info("FFP Listener started(key=" + key + ")");
		}

		_logger.info("Simulator Listeners started");

		return true;

	}
}
