package com.formssi.service;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.formssi.bean.SimCTOReceiveBean;

@Component("SimBridgeConfigSvc")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SimBridgeConfigSvc {

	private String receiveAckMsgPath;
	
	private List<String> txIdList;
	
	
	private LinkedBlockingQueue<SimCTOReceiveBean> listenerCommonQueue;
	
	private Integer messageCount;
	
	
	public LinkedBlockingQueue<SimCTOReceiveBean> getListenerCommonQueue() {
		if(listenerCommonQueue == null){
			listenerCommonQueue = new LinkedBlockingQueue<>();
		}
		return listenerCommonQueue;
	}

	public Integer getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(Integer messageCount) {
		this.messageCount = messageCount;
	}

	public String getReceiveAckMsgPath() {
		return receiveAckMsgPath;
	}

	public void setReceiveAckMsgPath(String receiveAckMsgPath) {
		this.receiveAckMsgPath = receiveAckMsgPath;
	}

	public List<String> getTxIdList() {
		return txIdList;
	}

	public void setTxIdList(List<String> txIdList) {
		this.txIdList = txIdList;
	}

}
