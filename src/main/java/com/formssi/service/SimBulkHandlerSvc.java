package com.formssi.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formssi.bean.FileDataBean;
import com.formssi.bean.SimBulkSendBean;
import com.formssi.business.helper.SimFileReadHelper;
import com.formssi.connector.sender.ISimMqSenderAgentInterface;
import com.formssi.connector.sender.mq.SimIbmWebSphereMqSenderAgent;
import com.formssi.defines.SimConstants;
import com.formssi.form.FileInfoForm;
import com.formssi.utils.SimFileHandleUtils;
import com.formssi.utils.SimStringUtils;

@Service
public class SimBulkHandlerSvc {

	private static final Logger _logger = LoggerFactory.getLogger(SimBulkHandlerSvc.class);
	
	@Autowired
	private SimFileReadHelper fileReadHelper;
	
	@Autowired
	private SimBridgeConfigSvc bridgeConfig;
	
	
	public List<FileInfoForm> parseToXml(String path) {
		List<FileInfoForm> infoList = new ArrayList<>();
		try {
			//read xlsx file to mapList bean
			FileDataBean fb = SimFileHandleUtils.readFile(new FileInputStream(path));
			
			// set directory name
			if (!SimStringUtils.isEmptyOrNull(path)){
				String dirName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
				fb.setDirName(dirName);
				
				//create receive path and excel file
				String recPath = SimConstants.BANKSIM_PATH_RECEIVE_ACK + dirName;
				File recDir = new File(recPath);
				if(recDir.exists()){
					SimFileHandleUtils.deleteDir(recDir);
				}

				if(recDir.mkdirs()){
					SimFileHandleUtils.createReceiveFile(dirName);
					bridgeConfig.setReceiveAckMsgPath(recPath);
					bridgeConfig.getListenerCommonQueue().clear();
					bridgeConfig.setMessageCount(null);
				}else{
					_logger.error("Create Receive Directory Faild");
				}
			
			}
			
			if (fb != null) {
				boolean helper = fileReadHelper.helpFileRead(fb);
				
				if (helper) {
					// read from BANKSIM_PATH_SEND get filename list to show
					String dirName = SimConstants.BANKSIM_PATH_SEND_REQ
							+ path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
					
					File dir = new File(dirName);
					if (dir.exists()) {
						String[] files = dir.list();
						List<String> txIdList = new ArrayList<>();
						for (String filename : files) {
							if (!SimStringUtils.isEmptyOrNull(filename)) {
								FileInfoForm bean = new FileInfoForm();
								
								String txId = filename.split("_")[1];
								bean.setTxId(txId);
								bean.setFileName(filename);
								bean.setCrtTm(filename.split("_")[2].substring(0, 14));
								infoList.add(bean);
								
								txIdList.add(txId);
							}
						}
						bridgeConfig.setTxIdList(txIdList);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return infoList;
	}
	

	public Boolean bulkSend(String dirName) {
		ISimMqSenderAgentInterface senderAgent = null;
		
		LinkedBlockingQueue<SimBulkSendBean> queue = readXmlFiles(dirName);
		
		try {
			senderAgent = SimSenderAgentSvc.getSenderAgent(SimConstants.CONNECTOR_NAME);
			if(senderAgent instanceof SimIbmWebSphereMqSenderAgent){
				SimIbmWebSphereMqSenderAgent ibmSender = (SimIbmWebSphereMqSenderAgent)senderAgent;
				
				if (queue != null && queue.size()>0) 
					while (true) {
						SimBulkSendBean simBulkSendBean = queue.poll();
						if (simBulkSendBean == null) {
							break;
						}
						
						ibmSender.bulkSendRequest(simBulkSendBean.getMsg(), simBulkSendBean.getPriority());
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}

	private LinkedBlockingQueue<SimBulkSendBean> readXmlFiles(String dirName) {
		
		LinkedBlockingQueue<SimBulkSendBean> queue = new LinkedBlockingQueue<>();
		
		if (!SimStringUtils.isEmptyOrNull(dirName)) {
			File dir = new File(dirName);
			if (dir.exists()) {
				File[] files = dir.listFiles();
				for (File f : files) {
					SimBulkSendBean sbsb = new SimBulkSendBean();
					BufferedReader bufr = null;
					try {
						bufr = new BufferedReader(new FileReader(f));
						StringBuffer sb = new StringBuffer();
						String line = null;
						while ((line = bufr.readLine()) != null) {
							sb.append(line + "\n");
						}
						// add to queue
						sbsb.setMsg(sb.toString());
						String payType = f.getName().split("_")[0].substring(3);
						if(!SimStringUtils.isEmptyOrNull(payType)){
							if(payType.matches("[a-zA-Z]01")){
								sbsb.setPriority(SimConstants.MQ_LEVEL_PRIORITY_HIGH);
							}else{
								sbsb.setPriority(SimConstants.MQ_LEVEL_PRIORITY_MEDIUM);
							}
						}
						queue.add(sbsb);

					} catch (IOException e) {
						_logger.error("read file error", e);
					} finally {
						try {
							bufr.close();
						} catch (IOException e) {
							_logger.error("close Stream error", e);
						}
					}
				}
			}
		}
		
		return queue;
	}

//	private SimBulkSendContext initConnector(SimMqConfig mqConfig) {
//		SimBulkSendContext context = new SimBulkSendContext();
//		int qOptioin = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_INQUIRE | MQC.MQOO_OUTPUT;
//		MQQueueManager qMgr = null;
//		MQQueue qQueue = null;
//		
//		SimQueueManagerConfig qmCfg = mqConfig.getQmCfg();
//
//		MQEnvironment.hostname = qmCfg.getHostName(); 
//		MQEnvironment.port = qmCfg.getPort(); 
//		MQEnvironment.channel = qmCfg.getClientChannel();
//		MQEnvironment.CCSID = qmCfg.getCcsid();
//		// MQ中拥有权限的用户名
//		MQEnvironment.userID = qmCfg.getUser();
//		// 用户名对应的密码
//		MQEnvironment.password = qmCfg.getPassword();
//		try {
//			qMgr = new MQQueueManager(qmCfg.getQueueManagerName());
//			if (qMgr != null)
//				context.setqMgr(qMgr);
//
//			qQueue = qMgr.accessQueue(mqConfig.getDefaultDestinationQueueName(), qOptioin);
//
//			if (qQueue != null)
//				context.setqQueue(qQueue);
//		} catch (MQException e) {
//			if (_logger.isErrorEnabled()) {
//				_logger.error("Init Connector Exception.:", e);
//			}
//			return null;
//		}
//		return context;
//	}

//	@Async("asyncServiceExecutor")
//	private void execute(LinkedBlockingQueue<String> queueMsgData, SimBulkSendContext context) {
//
//		_logger.info("==================START EXECUTEASYNC==================");
//		String msg = "";
//		MQQueue qQueue = context.getqQueue();
//		MQQueueManager qMgr = context.getqMgr();
//		try {
//			while (true) {
//				msg = queueMsgData.poll();
//				if (msg == null) {
//					break;
//				}
//				// send to fps
//				MQMessage qMsg = new MQMessage();
//				MQPutMessageOptions pmo = new MQPutMessageOptions();
//
//				qMsg.write(msg.getBytes("GBK"));
//				qQueue.put(qMsg, pmo);
//			}
//		} catch (Exception e) {
//			_logger.error("Send To IBM MQ Occur Errors", e);
//		} finally {
//			try {
//				qQueue.close();
//				qMgr.disconnect();
//			} catch (MQException e) {
//				_logger.error("A WebSphere MQ error occurred when close connect: Completion code " + e.completionCode
//						+ " Reason Code is " + e.reasonCode);
//			}
//		}
//
//		_logger.info("==================END EXECUTEASYNC==================");
//	}

}