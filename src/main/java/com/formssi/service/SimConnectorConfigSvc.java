package com.formssi.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.formssi.config.ISimConnectorConfig;
import com.formssi.config.mq.SimMqConfig;
import com.formssi.config.mq.SimQueueConfig;
import com.formssi.config.mq.SimQueueManagerConfig;
import com.formssi.defines.SimConstants;

public class SimConnectorConfigSvc {

	private static Logger _logger = LoggerFactory.getLogger(SimConnectorConfigSvc.class);

	private static SimConnectorConfigSvc instance = null;

	private Map<String, ISimConnectorConfig> connectConfigMap;

	public static SimConnectorConfigSvc getInstance() throws Exception {
		if (instance == null) {
			instance = new SimConnectorConfigSvc();
		}
		return instance;
	}

	public SimConnectorConfigSvc() throws Exception {
		try {
			initConnectorConfig();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void initConnectorConfig() throws Exception {

		if (this.connectConfigMap == null) {
			this.connectConfigMap = new HashMap<String, ISimConnectorConfig>();
		}

		this.connectConfigMap.clear();

		// read connector.properties file
		// PropertiesFile loc_rootPro =
		// FFPRuntimeConfigSvc.getInstance().getRootConfig();
		// PropertiesFile loc_connectorPro =
		// FFPRuntimeConfigSvc.getInstance().getPropertiesFile(loc_rootPro.get("config.connector.filename"));

		// String connectorKey = "connector.key.{index}".replace(".",
		// "\\.").replace("{index}", "([a-zA-Z0-9]+)");

		// for (String key : loc_rootPro.keySet()){
		// if (key.matches(connectorKey)){

		Properties loc_connectorPro = PropertiesLoaderUtils.loadAllProperties("connector.properties");

		try {
//			String connectorRefName = loc_rootPro.get(key);
			String value = (String) loc_connectorPro.get(SimConstants.CONNECTOR_NAME);
			String connectorType = (String) loc_connectorPro.get(value + ".mq.type");
			if (SimConstants.CONNECTTOR_TYPE_MQ_WEBPHEREMQ.equals(connectorType)) {

				SimMqConfig mqConfig = new SimMqConfig();
				mqConfig.setConnectorName(value);
				mqConfig.setConnectorType(connectorType);

				SimQueueManagerConfig queuemgrConfig = new SimQueueManagerConfig();
				queuemgrConfig.setHostName((String) loc_connectorPro.get(value + ".mq.hostname"));
				queuemgrConfig.setPort(new Integer((String) loc_connectorPro.get(value + ".mq.port")));
				queuemgrConfig.setQueueManagerName((String)loc_connectorPro.get((String)value + ".mq.queueManagerName"));
				queuemgrConfig.setCcsid(new Integer((String)loc_connectorPro.get((String)value + ".mq.ccsid")));
				
				queuemgrConfig.setSslEnable(new Boolean((String)loc_connectorPro.get((String)value + ".mq.ssl.enable")));
				queuemgrConfig.setSslCipherSuite((String)loc_connectorPro.get((String)value + ".mq.ssl.cipherSuite"));
				queuemgrConfig.setSslPeerName((String)loc_connectorPro.get((String)value + ".mq.ssl.peerName"));
				queuemgrConfig.setSslTrustStoreFilename((String)loc_connectorPro.get((String)value + ".mq.ssl.trustStore"));
				queuemgrConfig.setSslTrustStorePassword((String)loc_connectorPro.get((String)value + ".mq.ssl.trustStorePassword"));
				queuemgrConfig.setSslKeyStoreFilename((String)loc_connectorPro.get((String)value + ".mq.ssl.keyStore"));
				queuemgrConfig.setSslKeyStorePassword((String)loc_connectorPro.get((String)value + ".mq.ssl.keyStorePassword"));
				
				queuemgrConfig.setSubscribeInward(new Boolean((String)loc_connectorPro.get((String)value + ".mq.in.subscribe")));
				queuemgrConfig.setClientChannel((String)loc_connectorPro.get((String)value + ".mq.clientChannel"));
				queuemgrConfig.setUser((String)loc_connectorPro.get((String)value + ".mq.user"));
				queuemgrConfig.setPassword((String)loc_connectorPro.get((String)value + ".mq.password"));
				mqConfig.setSessionCacheSize(new Integer((String) loc_connectorPro.get(value + ".mq.sessionCacheSize")));

				if (_logger.isInfoEnabled()) {
					_logger.info("Queue Manager configuration loaded. Reference: [{}] QM: [{}]", value,
							queuemgrConfig.getQueueManagerName());
				}

				mqConfig.setQmCfg(queuemgrConfig);

				SimQueueConfig queueConfig1 = new SimQueueConfig();
				String str1 = (String) loc_connectorPro.get(value + ".send.req." + SimConstants.MQ_LEVEL_PRIORITY_HIGH);
				if (str1 == null)
					_logger.error(value + ".send.req." + SimConstants.MQ_LEVEL_PRIORITY_HIGH + " must be setup");
				queueConfig1.setQueueName(str1);
				mqConfig.setOutwardRequestHighPriorityQueueName(queueConfig1);

				SimQueueConfig queueConfig2 = new SimQueueConfig();
				String str2 = (String) loc_connectorPro.get(value + ".send.req." + SimConstants.MQ_LEVEL_PRIORITY_MEDIUM);
				if (str2 == null)
					_logger.error(value + ".send.req." + SimConstants.MQ_LEVEL_PRIORITY_MEDIUM + " must be setup");
				queueConfig2.setQueueName(str2);
				mqConfig.setOutwardRequestMediumPriorityQueueName(queueConfig2);

				SimQueueConfig queueConfig3 = new SimQueueConfig();
				String str3 = (String) loc_connectorPro.get(value + ".send.ack." + SimConstants.MQ_LEVEL_PRIORITY_HIGH);
				if (str3 == null)
					_logger.error(value + ".send.ack." + SimConstants.MQ_LEVEL_PRIORITY_HIGH + " must be setup");
				queueConfig3.setQueueName(str3);
				mqConfig.setOutwardAcknowledgeHighPriorityQueueName(queueConfig3);

				SimQueueConfig queueConfig4 = new SimQueueConfig();
				String str4 = (String) loc_connectorPro.get(value + ".send.ack." + SimConstants.MQ_LEVEL_PRIORITY_MEDIUM);
				if (str4 == null)
					_logger.error(value + ".send.ack." + SimConstants.MQ_LEVEL_PRIORITY_MEDIUM + " must be setup");
				queueConfig4.setQueueName(str4);
				mqConfig.setOutwardAcknowledgeMediumPriorityQueueName(queueConfig4);

				mqConfig.setDefaultDestinationQueueName((String) loc_connectorPro.get(value + ".send.default.queue"));

				if (queuemgrConfig.getSubscribeInward()) {
					String receivequeueRegex = ("(" + value + ")").replace(".", "\\.")
							+ "(\\.receive\\.)(req|ack)(\\.)([0-9a-zA-Z]+)";

					Map<String, SimQueueConfig> receiveQueueNameList = new HashMap<String, SimQueueConfig>();
					for (Object queuekey : loc_connectorPro.keySet()) {
						if (((String) queuekey).matches(receivequeueRegex)) {

							SimQueueConfig queueConfig = new SimQueueConfig();
							String[] arrry = ((String)loc_connectorPro.get(queuekey)).split(",");
							queueConfig.setQueueName(arrry[0].trim());
							queueConfig.setListener(arrry[1].trim());
							queueConfig.setThreadPoolSize(Integer.valueOf(arrry[2].trim()));
							queueConfig.setPriority(((String)queuekey).substring(((String)queuekey).lastIndexOf(".") + 1));
							receiveQueueNameList.put((String)queuekey, queueConfig);
						}
					}
					mqConfig.setReceiveQueueNameMap(receiveQueueNameList);
				}
				this.connectConfigMap.put(SimConstants.CONNECTOR_NAME, mqConfig);
			}
		} catch (Exception e) {
			if (_logger.isErrorEnabled()) {
				_logger.error(String.format("Failed to map QM properties '%s'", new Object[] { SimConstants.CONNECTOR_NAME }), e);
			}
		}
	}
	// }
	// }

	public Map<String, ISimConnectorConfig> getConnectorConfigMap() {
		return this.connectConfigMap;
	}
}
