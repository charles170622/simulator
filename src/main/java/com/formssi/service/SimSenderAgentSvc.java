package com.formssi.service;

import java.util.HashMap;
import java.util.Map;

import com.formssi.config.ISimConnectorConfig;
import com.formssi.config.mq.SimMqConfig;
import com.formssi.connector.sender.ISimMqSenderAgentInterface;
import com.formssi.connector.sender.mq.SimIbmWebSphereMqSenderAgent;
import com.formssi.defines.SimConstants;

public class SimSenderAgentSvc{
	
	private static Map<String, ISimMqSenderAgentInterface> senderAgentMap ;
	
	public static ISimMqSenderAgentInterface getSenderAgent(String senderName) throws Exception{
		
		if(senderAgentMap == null){
			senderAgentMap = new HashMap<String, ISimMqSenderAgentInterface>();
		}
		
		if(senderAgentMap.get(senderName) == null){
			ISimConnectorConfig config = SimConnectorConfigSvc.getInstance().getConnectorConfigMap().get(senderName);
			if(SimConstants.CONNECTTOR_TYPE_MQ_WEBPHEREMQ.equals(config.getConnectorType())){
				SimIbmWebSphereMqSenderAgent agent = new SimIbmWebSphereMqSenderAgent((SimMqConfig)config);
				senderAgentMap.put(senderName, agent);
			}else{
				return null;
			}
		}
		return senderAgentMap.get(senderName);
	}
	
}
