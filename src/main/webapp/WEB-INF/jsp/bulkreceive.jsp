<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>home</title>
<link rel="stylesheet" type="text/css"
	href="easyui/css/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/css//icon.css">
<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
<script type="text/javascript" src="easyui/js/jquery.min.js"></script>
<script type="text/javascript" src="easyui/js/jquery.easyui.min.js"></script>
</head>
<body>
	<h2>Send Message</h2>
	<div align="center">
		<div class="easyui-panel" title="File input:"
			style="width: 100%; max-width: 600px; padding: 30px 60px;">
			<div style="margin-bottom: 20px">
				<input id="file" class="easyui-filebox" style="width: 100%"> 
				<div id="generateInfo" style="display: none"></div>
			</div>
			<div>
				<a id="generateLinkBtn" onclick="generateXml()" class="easyui-linkbutton"
					style="width: 100%; height: 32px" >Generate XML Files</a>
			</div>
			<br/>
			<div>
				<a id="sendLinkBtn" onclick="sendXml()" class="easyui-linkbutton"
					style="width: 100%; height: 32px" >Send XML Files</a>
			</div>
		</div>
	</div>
	<br/><br/>
	<div align="center">
		<table id="fileListTable"></table>
		<div id="fileDetailDg" style="overflow:hidden;"></div>
	</div>

	<script type="text/javascript">
	
	$(function(){
		$("#sendLinkBtn").linkbutton('disable');
		
	});
	
	function sendXml(){
		
		var path = $("#file").filebox('getValue');
		var name = path.split("\\fakepath\\")[1];
    	var dirName = name.substring(0, name.indexOf("."));
    	
    	$.ajax({
            url: '/async?dirName=' + dirName,
            dataType:"json",
            success:function(data){
               alert(data.returnMsg);
            }
        });
		
	};	
	
	 //分页功能    
    function pagerFilter(data) {
        if (typeof data.length == 'number' && typeof data.splice == 'function') {
            data = {
                total: data.length,
                rows: data
            }
        }
        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            onSelectPage: function (pageNum, pageSize) {
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh', {
                    pageNumber: pageNum,
                    pageSize: pageSize
                });
                dg.datagrid('loadData', data);
            }
        });
        if (!data.originalRows) {
            if(data.rows)
                data.originalRows = (data.rows);
            else if(data.data && data.data.rows)
                data.originalRows = (data.data.rows);
            else
                data.originalRows = [];
        }
        var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));
        return data;
    } 
	
	function generateXml() {
		
		var path = $("#file").filebox('getValue');
		var nameExt = path.split(".")[1];
		
		$("#generateInfo").html("");
		
		if(path == ""){
			var info = "<span style='color:red'>no file</span>";
			$("#generateInfo").append(info).css("display", "");
			return;
		}
		if (!(nameExt == "xlsx" || nameExt == "csv")) {
			var info = "<span style='color:red'>error file format</span>";
			$("#generateInfo").append(info).css("display", "");
			return;
		}
		
		//grey button
		$("#generateLinkBtn").linkbutton('disable');
		$("#sendLinkBtn").linkbutton('disable');
		
		//read file
		$('#fileListTable').datagrid({    
		    url:'/fileRead' ,
		    rownumbers: true,
		    striped: true,
		    pagination: true,
		    queryParams: {
				path: path,
			},
		    pageSize: 10,
            pageList: [10,50,100,300,500], 
		    idField: "txId",
		    method: "POST",
		    fitColumns: true,
		    loadMsg: 'Handling files, please wait……',
		    singleSelect: true,
		    loadFilter: pagerFilter,
		    columns: [[    
		        {field:'txId',title:'Transaction ID',align: 'center', width: 100},    
		        {field:'fileName',title:'File Name',align: 'center', width:100},    
		        {field:'crtTm',title:'Create Time',align: 'center', width:100}    
		    ]],
		    onLoadSuccess: function(data){
		    	$("#generateLinkBtn").linkbutton('enable');
		    	$("#sendLinkBtn").linkbutton('enable');
		    },
		    onClickRow : function(index, row){
		    	var name = path.split("\\fakepath\\")[1];
		    	var dirName = name.substring(0, name.indexOf("."));
		    	
		    	$("#fileDetailDg").dialog({
		    		title: 'Detail',    
		    	    width: 1000,    
		    	    height: 600,    
		    	    closed: false,    
		    	    cache: false,    
		    	    modal: true,
		    	    onOpen : function() {
		                $.ajax({
		                    url: '/show?dirName=' + dirName + "&fileName=" + row.fileName,
		                    dataType:"json",
		                    success:function(data){
		                        $('#fileDetailDg').empty();
		                        var contents = data.returnMsg;
		                        var text = "<textarea rows=\"38\" cols=\"138\" readonly=\"readonly\">"+contents+"</textarea>";
		                        $('#fileDetailDg').append(text);
		                    }
		                });

		            }
		    	});
		    },
		});  

	};
	</script>
</body>
</html>