<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>home</title>
<link rel="stylesheet" type="text/css"
	href="easyui/css/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/css//icon.css">
<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
<script type="text/javascript" src="easyui/js/jquery.min.js"></script>
<script type="text/javascript" src="easyui/js/jquery.easyui.min.js"></script>
</head>
<body>
	<input type="hidden" id="dirName" value="${dirName}">
	<h2>Send Message</h2>
	<div align="center">
		<div>
			<a id="backBtn" href="javascript:history.back()" class="easyui-linkbutton"
				style="width: 40%; height: 40px; " >Back</a>
		
		
			<a id="sendLinkBtn" onclick="sendXml()" class="easyui-linkbutton"
				style="width: 40%; height: 40px; " >Send</a>
		</div>
	</div>
	<br/><br/>
	<div align="center">
		<table id="fileListTable"></table>
		<div id="fileDetailDg" style="overflow:hidden;"></div>
	</div>

	<script type="text/javascript">
	var dirName = $("#dirName").val();
	var directory = dirName.substring(0, dirName.indexOf("."));
	
	$(function(){
		
		$("#backBtn").linkbutton('disable');
		$("#sendLinkBtn").linkbutton('disable');
		
		$('#fileListTable').datagrid({    
		    url:'/fileRead' ,
		    rownumbers: true,
		    striped: true,
		    pagination: true,
		    queryParams: {
				dirName: dirName,
			},
		    pageSize: 10,
            pageList: [10,50,100,300,500], 
		    idField: "txId",
		    method: "POST",
		    fitColumns: true,
		    loadMsg: 'Handling files, please wait……',
		    singleSelect: true,
		    loadFilter: pagerFilter,
		    columns: [[    
		        {field:'txId',title:'Transaction ID',align: 'center', width: 100},    
		        {field:'fileName',title:'File Name',align: 'center', width:100},    
		        {field:'crtTm',title:'Create Time',align: 'center', width:100}    
		    ]],
		    onLoadSuccess: function(data){
		    	$("#backBtn").linkbutton('enable');
		    	$("#sendLinkBtn").linkbutton('enable');
		    },
		    onClickRow : function(index, row){
		    	$("#fileDetailDg").dialog({
		    		title: 'Detail',    
		    	    width: 1000,    
		    	    height: 600,    
		    	    closed: false,    
		    	    cache: false,    
		    	    modal: true,
		    	    onOpen : function() {
		                $.ajax({
		                    url: '/show?directory=' + directory + "&fileName=" + row.fileName,
		                    dataType:"json",
		                    success:function(data){
		                        $('#fileDetailDg').empty();
		                        var contents = data.returnMsg;
		                        var text = "<textarea rows=\"38\" cols=\"138\" readonly=\"readonly\">"+contents+"</textarea>";
		                        $('#fileDetailDg').append(text);
		                    }
		                });

		            }
		    	});
		    },
		});  
	});
	
	function sendXml(){
		
    	$.ajax({
            url: '/async?directory=' + directory,
            dataType:"json",
            success:function(data){
               alert(data.returnMsg);
            }
        });
		
	};	
	
	
	
	 //分页功能    
    function pagerFilter(data) {
        if (typeof data.length == 'number' && typeof data.splice == 'function') {
            data = {
                total: data.length,
                rows: data
            }
        }
        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            onSelectPage: function (pageNum, pageSize) {
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh', {
                    pageNumber: pageNum,
                    pageSize: pageSize
                });
                dg.datagrid('loadData', data);
            }
        });
        if (!data.originalRows) {
            if(data.rows)
                data.originalRows = (data.rows);
            else if(data.data && data.data.rows)
                data.originalRows = (data.data.rows);
            else
                data.originalRows = [];
        }
        var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));
        return data;
    } 
	
	
	</script>
</body>
</html>