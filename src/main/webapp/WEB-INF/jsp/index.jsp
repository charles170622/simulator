<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>home</title>
<link rel="stylesheet" type="text/css"
	href="easyui/css/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/css//icon.css">
<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
<script type="text/javascript" src="easyui/js/jquery.min.js"></script>
<script type="text/javascript" src="easyui/js/jquery.easyui.min.js"></script>
</head>
<body>
	<h2>Generate Xml Files</h2>
	<br/><br/><br/><br/>
	<form id="form" action="/xmllist" method="post">
		<input type="hidden" name="dirName" >
		<div align="center">
			<div class="easyui-panel" title="File input:"
				style="width: 100%; max-width: 600px; padding: 30px 60px;">
				<div style="margin-bottom: 20px">
					<input id="file" class="easyui-filebox" style="width: 100%"> 
					<div id="generateInfo" style="display: none"></div>
				</div>
				<div>
					<a id="generateLinkBtn" onclick="toXmlList()" class="easyui-linkbutton"
						style="width: 100%; height: 32px" >Generate XML Files</a>
				</div>
			</div>
		</div>
	</form>
	

	<script type="text/javascript">
	function toXmlList() {
		var path = $("#file").filebox('getValue');
		var nameExt = path.split(".")[1];
		
		$("#generateInfo").html("");
		
		if(path == ""){
			var info = "<span style='color:red'>no file</span>";
			$("#generateInfo").append(info).css("display", "");
			return;
		}
		if (!(nameExt == "xlsx" || nameExt == "csv")) {
			var info = "<span style='color:red'>error file format</span>";
			$("#generateInfo").append(info).css("display", "");
			return;
		}
		
		//window.location= $("#PageContext").val()+"/xmllist?dirName=" + path.split("\\")[2]; 
		$("input[name=dirName]").val(path.split("\\")[2]);
		$("#form").submit();
	};
	</script>
</body>
</html>