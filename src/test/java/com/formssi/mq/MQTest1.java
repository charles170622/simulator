package com.formssi.mq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.formssi.bean.SimCTOReceiveBean;
import com.formssi.msg.iclfps.creator.SimMsgCreator;
import com.formssi.msg.iclfps.creator.SimMsgProperty;
import com.formssi.utils.SimFileHandleUtils;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQQueueConnectionFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class MQTest1 {
	/**
	 * 队列管理器的名称
	 */
	private String qManagerName="MQ";
	
	/**
	 * 队列管理器
	 */
	private   MQQueueManager qMgr;
	/**
	 * 队列名称
	 */
	private  String queueName="FPE.P000.ACK.H.REM";
	/**
	 * 队列
	 */
	private MQQueue qQueue;
	
	/**
	 * mq服务器所在的主机名称
	 */
	private String hostname="192.168.213.130";
	/**
	 * 服务器连接通道名称
	 */
	private String channelName="CNN";
	
	/**
	 * 监听器监听的端口
	 */
	private  int  port = 1414;
	/**
	 * 传输的编码类型
	 */
	private  int CCSID = 1381; 
	
	private JmsTemplate jmsTemplate = new JmsTemplate();
	
	private String username = "admin";
	
	private String password = "123";
	
	private int sessionCacheSize = 30;
	
	private String defaultQueue = "PT.FPE.REQ.H01.LOC";
	
	private String ackQueueName = "FPE.P000.ACK.M.REM";
	
	private String reqQueueName = "FPE.P000.REQ.M.REM";
	
	@Test
	public void test(){
			try {
//				SimFileHandleUtils.createReceiveFile("1");
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
//	@Before
//	public  void  init(){
//        try {
//            MQEnvironment.hostname = this.hostname; // 安裝MQ所在的ip address
//            MQEnvironment.port = this.port; // TCP/IP port
//            MQEnvironment.channel = this.channelName;
//            MQEnvironment.CCSID = CCSID;
//            //MQ中拥有权限的用户名
//            MQEnvironment.userID = "admin";//"MUSR_MQADMIN";
//            //用户名对应的密码
//            MQEnvironment.password = "123";//"123456";
//            qMgr = new MQQueueManager(this.qManagerName);
//            int qOptioin = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_INQUIRE | MQC.MQOO_OUTPUT;
//            
//            qQueue = qMgr.accessQueue(queueName, qOptioin);
// 
//        } catch (MQException e) {
//        	e.printStackTrace();
//        }
//	}
	
	
 
 
    
    /**
     * 发送信息
     */
 
//    public void SendMsg(byte[] qByte) {
//      public void SendMsg(String msg) {
//        try {
//            MQMessage qMsg = new MQMessage();
////            qMsg.write(qByte);
//            qMsg.writeString(msg);
//            MQPutMessageOptions pmo = new MQPutMessageOptions();
//            qQueue.put(qMsg, pmo);
//            
//            System.out.println("The message is sent!");
////            System.out.println("\tThe message is " + new String(qByte, "GBK"));
//            
//        } catch (MQException e) {
//        	e.printStackTrace();
//            System.out
//                    .println("A WebSphere MQ error occurred : Completion code "
//                            + e.completionCode + " Reason Code is "
//                            + e.reasonCode);
//        } catch (java.io.IOException e) {
//        	e.printStackTrace();
//            System.out
//                    .println("An error occurred whilst to the message buffer "
//                            + e);
//        }
// 
//    }
    
    
 
    
    /**
     * 从消息队列取数据
     */
//    public void GetMsg() {
//        try {
//            MQMessage retrievedMessage = new MQMessage();
// 
//            MQGetMessageOptions gmo = new MQGetMessageOptions();
//            gmo.options += MQC.MQPMO_SYNCPOINT;
//            qQueue.get(retrievedMessage, gmo);
//            int length = retrievedMessage.getDataLength();
// 
//            byte[] msg = new byte[length];
// 
//            retrievedMessage.readFully(msg);
// 
//            String sMsg = new String(msg,"GBK");
//            System.out.println(sMsg);
// 
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        } catch (MQException e) {
//        	e.printStackTrace();
//            if (e.reasonCode != 2033) // 没有消息
//            {
//                e.printStackTrace();
//                System.out
//                        .println("A WebSphere MQ error occurred : Completion code "
//                                + e.completionCode
//                                + " Reason Code is "
//                                + e.reasonCode);
//            }
//        } catch (java.io.IOException e) {
//            System.out
//                    .println("An error occurred whilst to the message buffer "
//                            + e);
//        }
//    }
    
 
    
    
    
    /**
     * 单元测试方法
     */
    
//    @Test
//    public  void  testMQ(){
//    	MQTest1 mqst = new MQTest1();
//        mqst.init();
//        try {
//        	String msg = "";
//        	StringBuffer sb = new StringBuffer();
//        	//read file from local
//        	File file = new File("E:/SIM/TESTDATA/", "pacs002.xml");
//        	BufferedReader bufr = new BufferedReader(new FileReader(file));
//        	String line = null;
//        	while((line=bufr.readLine())!=null){
//        		sb.append(line);
//        	}
//        	msg = sb.toString();
//        	
//            mqst.SendMsg(msg);
////            mqst.GetMsg();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    
    @Test
    public void templateSendMsg() throws Exception{
    	BufferedReader bufr = null;
    	
        try {  
             
            MQQueueConnectionFactory factory = new MQQueueConnectionFactory();  
            factory.setQueueManager(this.qManagerName);  
            factory.setHostName(this.hostname);  
            factory.setTransportType(1);  
            factory.setPort(1414);  
            factory.setChannel(this.channelName);
            factory.setCCSID(this.CCSID);
            
            UserCredentialsConnectionFactoryAdapter adapter = new UserCredentialsConnectionFactoryAdapter();
			adapter.setTargetConnectionFactory(factory);
			adapter.setUsername(this.username);
			adapter.setPassword(this.password);

			CachingConnectionFactory ccf = new CachingConnectionFactory();
			ccf.setTargetConnectionFactory(adapter);
			ccf.setSessionCacheSize(this.sessionCacheSize);
			
			this.jmsTemplate.setConnectionFactory(ccf);
			this.jmsTemplate.setSessionTransacted(true);
			this.jmsTemplate.setPubSubDomain(false);
			this.jmsTemplate.setDefaultDestinationName("queue:///" + this.defaultQueue);
  
			SimMsgProperty msgProperty = new SimMsgProperty();
    		msgProperty.setPropertyMap(null);
            
            String msg = "";
       
//        	File dir = new File("E:/SIM/TESTDATA/2");
            
            File dir = new File("E:/SIM/TESTDATA/ACK/1");
            
        	File[] files = dir.listFiles();
        	for(int i=0; i<files.length; i++){
        		File file = files[i];
        		
        		StringBuffer sb = new StringBuffer();
        		bufr = new BufferedReader(new FileReader(file));
        		String line = null;
        		while((line=bufr.readLine())!=null){
        			sb.append(line + "\n");
        		}
        		msg = sb.toString();
        		SimMsgCreator msgCreator = new SimMsgCreator();
        		msgCreator.setMsgObj(msg);
        		
        		msgCreator.setMsgProperty(msgProperty);
        		msgCreator.setMsgConverter(this.jmsTemplate.getMessageConverter());
        		
//        		this.jmsTemplate.send(this.ackQueueName, msgCreator);
        		
        		this.jmsTemplate.send(this.reqQueueName, msgCreator);
        		
        		System.out.println("Sent message:\n" + msg);  
        	}
        	
        } catch (JMSException jmsex) {  
            jmsex.fillInStackTrace();  
        } finally {  
            try {  
            	bufr.close();
            } catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
    }   
  
    
    /**
     * 释放资源
     */
//    @After
//    public  void  release(){
//        try {
//            qQueue.close();
//            qMgr.disconnect();
//        } catch (MQException e) {
//            System.out
//                    .println("A WebSphere MQ error occurred : Completion code "
//                            + e.completionCode + " Reason Code is "
//                            + e.reasonCode);
//        }	
//
//}
}
